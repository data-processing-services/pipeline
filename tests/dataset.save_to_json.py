from context import pipeline
import itertools, json, random, gc, logging 

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)



# # load config and texts
json_config = json.loads(pipeline.read_file("../data/samples/configs/bdn.pipeconf.json"))
text_paths = pipeline.read_dir( "../data/samples/texts/txt/bdn/*.txt" )
# #text_paths = pipeline.read_dir( "datasamples/test/*.txt")


### CONVERT AND SAVE by INTERFACE CLASS - START
corpus = pipeline.corpus.Corpus({
    "name" : "bdn_test", 
    "author" : "Uwe Sikora"
    })


for item in text_paths:
    text = pipeline.read_file(item)
    res = pipeline.interfaces.Resource( {"name" : item } )
    pipe = res.convert(
        data = text,
        pipeline_config = json_config,
        pipeline_scope = globals(), 
        pipeline_mode = {
            "indices" : [0,1,2,3,4,5,6]
    })
    corpus.add_resource( res )

corpus.save( "../data/dumps/bdn.dump.corpus", form="text")
### CONVERT AND SAVE by INTERFACE CLASS - END

### CONVERT AND SAVE - START
# # run pipeline
# corpus = Corpus({
#     "name" : "testcorpus", 
#     "author" : "Uwe Sikora"
#     })

# for item in text_paths:
#     #print(item)
#     text = pipeline.read_file(item)
#     #print(text)
#     pipe = pipeline.Pipeline( json_config, globals() )
#     result = pipe.run( 
#         data = text, 
#         mode_arguments = {
#             "indices" : [0,1,2,3,4,5,6]
#         })
#     #print(result)
#     #print(type(item))
#     corpus.add_resource({
#         "metadata" : { 
#             "name" : item,
#             "lemmatised" : True,
#             "removed_stopwords" : True, 
#             }, 
#         "data" : result
#         })


# pprint.pprint(corpus.resources[1].data)

# corpus.save("dumps/corpora/bdn")
### CONVERT AND SAVE - END



### LOAD Corpus - START
corpus = pipeline.corpus.Corpus.load("../data/dumps/bdn.pickle.corpus")
#print(corpus.__dict__)
#print(dir(corpus))
print([ doc.data for doc in corpus])
### LOAD Corpus - END

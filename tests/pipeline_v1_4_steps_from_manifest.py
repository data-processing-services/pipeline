from context import pipeline
import glob, sys, os, time, json, inspect, pprint, logging

# sys.path.append("../../nlp-package-1.0.0")
# import nlp

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

#####***~~-
##################*~{ Test Functions }~*##################***~~-
#####***~~-

def return_a_str_instead_of_a_python_list( data, separator ):
    return separator.join(data)


#####***~~-
##################*~{ Config }~*##################***~~-
#####***~~-

pipeline_config_dict = {
    "contexts" : ["../../nlp-package-1.0.0"],
    "mode" : {
        "indices" : [0, 1, 2, 3, 4, 5, 6]
    },
    "steps" : [
        {
            "id": "tokenizer-abbr",
            "function": {
                "name" : "tokenizeAbbr",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "regex" : "\\w+-\\w+|\\w+|\\S+"
            } 
        },
        {
            "id": "regex-cleaner",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "[\\.;]?\\[\\d\\]|\\W{2}" }
        },
        {
            "id": "remove-punctuation",
            "function" : {
                "name" : "removePunctuation",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "remove-stopwords",
            "function": {
                "name" : "removeStopwords",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "stopwords" : {
                    "name" : "stopwords",
                    "module" : "nlp.languages.german"
                }
            },
            "contexts" : ["../../nlp-package-1.0.0", "../../aggrepy-1.0.0"]
        },
        {
            "id" : "lemmatise-by-cab",
            "function" : {
                "name" : "cab_lemmatiser",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "lower-cases",
            "function" : {
                "name" : "transformCases",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "clean-up",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "\\d+|\\d+[a-z]+|^\\w{2}$" }
        },
        {
            "id": "join-the-bag-of-words",
            "function_object": return_a_str_instead_of_a_python_list,
            "arguments" : {
                "separator" : " - "
            }
        }
    ]
}

#pipeline_config_json = json.dumps( pipeline_config_dict )

# step_config_dicts = [
#     {
#         "id": "tokenizer-abbr",
#         "function_object": nlp.functions.tokenizeAbbr,
#         "arguments" : {
#             "regex" : "\\w+-\\w+|\\w+|\\S+"
#         }
#     },
#     {
#         "id": "regex-cleaner",
#         "function_object": nlp.functions.regex_cleaner,
#         "arguments" : {
#             "regex": "[\\.;]?\\[\\d\\]|\\W{2}"
#         }
#     },
#     {
#         "id": "remove-punctuation",
#         "function_object": nlp.functions.removePunctuation
#     },
#     {
#         "id": "remove-stopwords",
#         "function_object": nlp.functions.removeStopwords,
#         "arguments" : {
#             "stopwords" : nlp.languages.german.stopwords
#         }
#     },
#     {
#         "id": "remove-stopwords-name",
#         "function_object": nlp.functions.removeStopwords,
#         "arguments" : {
#             "stopwords" : {
#                     "name" : "stopwords",
#                     "module" : "nlp.languages.german"
#                 }
#         }
#     },
#     {
#         "id": "lemmatise-by-cab",
#         "function_object": nlp.functions.cab_lemmatiser
#     },
#     {
#         "id": "lower-cases",
#         "function" : {
#             "name" : "transformCases",
#             "module" : "nlp.functions"
#         }
#     },
#     {
#         "id": "clean-up",
#         "function_object": nlp.functions.regex_cleaner,
#         "arguments" : {
#             "regex": "\\d+|\\d+[a-z]+|^\\w{2}$"
#         }
#     },
#     {
#         "id": "join-the-bag-of-words",
#         "function_object": return_a_str_instead_of_a_python_list,
#         "arguments" : {
#             "separator" : " - "
#         }
#     }
# ]

#####***~~-
##################*~{ TESTING }~*##################***~~-
#####***~~-


print("\n\n### CREATE STEP FROM MANIFEST ###")
step = pipeline.Step.from_manifest({
    "id": "join-the-bag-of-words-direct-add",
    "function": {
        "name" : "removeStopwords",
        "module" : "nlp.functions"
    },
    "arguments" : {
        "separator" : " - ",
        "stopwords" : {
            "name" : "stopwords",
            "module" : "nlp.languages.german"
        }
    },
    "contexts" : ["../../nlp-package-1.0.0", "../../aggrepy-1.0.0"]
})

print("STEP:", step.__dict__)
print("CONFIG", step.config)
print("MANIFEST", step.manifest)
print("FUNCTION", step.function)
print("CALLABLE", step.function_object )
print("ARGUMENTS", step.arguments)


print("\n\n### STEP BY FRAGMENTS ###")
step_2 = pipeline.Step(
    contexts = ["../../nlp-package-1.0.0", "../../aggrepy-1.0.0"]
)

step_2.id = "somewhat-step"
print(step_2.function_object)
print(step_2.function)

step_2.function = {
    "name" : "transformCases",
    "module" : "nlp.functions"
}

print(step_2.function_object)
print(step_2.function)

step_2.arguments = {
    "separator" : " - ",
    "stopwords" : {
        "name" : "stopwords",
        "module" : "nlp.languages.german"
    }
}

print("CALLABLE", step_2.function_object)
print("FUNKTION", step_2.function)
print("ARGUMENTS", step_2.arguments)
print("MANIFEST", step_2.manifest)
print("CONTEXTS", step_2.contexts)


print("\n\n### CREATE STEP MANIFEST ###")
print("ID", step_2.id)
print("DOC", step_2.doc)
step_2_manifest = step_2.manifest
print("MANIFEST", step_2_manifest)


print("\n\n### CREATE STEP FROM MANIFEST AGAIN ###")
step_2_b = pipeline.Step.from_manifest( step_2_manifest )
print("STEP:", step_2_b.__dict__)
print("CONFIG", step_2_b.config)
print("MANIFEST", step_2_b.manifest)
print("FUNCTION", step_2_b.function)
print("CALLABLE", step_2_b.function_object )
print("ARGUMENTS", step_2_b.arguments)
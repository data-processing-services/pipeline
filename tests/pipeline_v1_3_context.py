from context import pipeline
import glob, logging, sys, os, time, json, inspect, pprint

# sys.path.append("../../nlp-package-1.0.0")
# import nlp
# sys.path.append("../../aggrepy-1.0.0")
# import aggrepy

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

#####***~~-
##################*~{ Definitions }~*##################***~~-
#####***~~-



#pprint.pprint(globals())

def t():
    return t 

#print( pipeline.get_funct("t") )
#print( pipeline.get_funct("nlp.functions.core.regex_cleaner") )
#print( pipeline.get_module( pipeline.main_script(), "nlp.functions.core") )


context = pipeline.Context("../../nlp-package-1.0.0", "../../aggrepy-1.0.0")
#context = pipeline.Context()
#print(context.__dict__)

#module = context.get_module("nlp.functions.core")
#print(module.__dict__)

sym = context.get_symbol("regex_cleaner", "nlp.functions.core")
print(sym)

module = context.get_module("aggrepy")
print(module.__dict__)

module = context.import_module("nlp")
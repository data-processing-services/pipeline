from context import pipeline
import glob, sys, os, time, json, inspect, pprint, logging

# sys.path.append("../../nlp-package-1.0.0")
# import nlp

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

#####***~~-
##################*~{ Test Functions }~*##################***~~-
#####***~~-

def return_a_str_instead_of_a_python_list( data, separator ):
    return separator.join(data)


#####***~~-
##################*~{ Config }~*##################***~~-
#####***~~-

pipeline_config_dict = {
    "contexts" : ["../../nlp-package-1.0.0"],
    "mode" : {
        "indices" : [0, 1, 2, 3, 4, 5, 6]
    },
    "steps" : [
        {
            "id": "tokenizer-abbr",
            "function": {
                "name" : "tokenizeAbbr",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "regex" : "\\w+-\\w+|\\w+|\\S+"
            } 
        },
        {
            "id": "regex-cleaner",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "[\\.;]?\\[\\d\\]|\\W{2}" }
        },
        {
            "id": "remove-punctuation",
            "function" : {
                "name" : "removePunctuation",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "remove-stopwords",
            "function": {
                "name" : "removeStopwords",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "stopwords" : {
                    "name" : "stopwords",
                    "module" : "nlp.languages.german"
                }
            },
            "contexts" : ["../../nlp-package-1.0.0", "../../aggrepy-1.0.0"]
        },
        {
            "id" : "lemmatise-by-cab",
            "function" : {
                "name" : "cab_lemmatiser",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "lower-cases",
            "function" : {
                "name" : "transformCases",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "clean-up",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "\\d+|\\d+[a-z]+|^\\w{2}$" }
        }
    ]
}

pipeline_config_json = json.dumps( pipeline_config_dict )

# step_config_dicts = [
#     {
#         "id": "tokenizer-abbr",
#         "function_object": nlp.functions.tokenizeAbbr,
#         "arguments" : {
#             "regex" : "\\w+-\\w+|\\w+|\\S+"
#         }
#     },
#     {
#         "id": "regex-cleaner",
#         "function_object": nlp.functions.regex_cleaner,
#         "arguments" : {
#             "regex": "[\\.;]?\\[\\d\\]|\\W{2}"
#         }
#     },
#     {
#         "id": "remove-punctuation",
#         "function_object": nlp.functions.removePunctuation
#     },
#     {
#         "id": "remove-stopwords",
#         "function_object": nlp.functions.removeStopwords,
#         "arguments" : {
#             "stopwords" : nlp.languages.german.stopwords
#         }
#     },
#     {
#         "id": "remove-stopwords-name",
#         "function_object": nlp.functions.removeStopwords,
#         "arguments" : {
#             "stopwords" : {
#                     "name" : "stopwords",
#                     "module" : "nlp.languages.german"
#                 }
#         }
#     },
#     {
#         "id": "lemmatise-by-cab",
#         "function_object": nlp.functions.cab_lemmatiser
#     },
#     {
#         "id": "lower-cases",
#         "function" : {
#             "name" : "transformCases",
#             "module" : "nlp.functions"
#         }
#     },
#     {
#         "id": "clean-up",
#         "function_object": nlp.functions.regex_cleaner,
#         "arguments" : {
#             "regex": "\\d+|\\d+[a-z]+|^\\w{2}$"
#         }
#     },
#     {
#         "id": "join-the-bag-of-words",
#         "function_object": return_a_str_instead_of_a_python_list,
#         "arguments" : {
#             "separator" : " - "
#         }
#     }
# ]



#####***~~-
##################*~{ TEST }~*##################***~~-
#####***~~-
# def copy_dict( input_dict, ignore = [] ):
#     return update_dict( input_dict, {}, ignore )

# def update_dict( input_dict, update_dict, ignore = [] ):
#     for key, value in input_dict.items():
#         if key not in ignore:
#             update_dict[ key ] = value
#     return update_dict

# def function_from_context( context, funct_manifest ):
#     funct_name = funct_manifest.get( 'name' )
#     module_name = funct_manifest.get( 'module' )
#     return context.get_symbol( funct_name, module_name )


# def argument_from_context( context, argument_manifest ):
#     argument_name = argument_manifest.get( 'name' )
#     module_name = argument_manifest.get( 'module' )
#     return context.get_symbol( argument_name, module_name )


# def step_config_from_manifest( context, step_manifest ):
#     dispatched_function = {}
#     dispatched_function['manifest'] = copy_dict( step_manifest, ignore = ["function", "function_object", "contexts"] )

#     # disptach steps
#     if step_manifest.get("function"):
#         #print("function by name")
#         dispatched_function['manifest']['function'] = step_manifest['function']
#         dispatched_function['function_object'] = function_from_context( context, step_manifest['function'] )
    
#     elif step_manifest.get("function_object"):
#         #print("function by symbol")
#         dispatched_function['function_object'] = step_manifest['function_object']
#         dispatched_function['manifest']['function'] = {
#             "name" : step_manifest['function_object'].__name__,
#             "module" : step_manifest['function_object'].__module__
#         }
    
#     # disptach arguments
#     arguments = step_manifest.get( 'arguments' )
#     if arguments:
#         dispatched_function['arguments'] = {}

#         for key, value in arguments.items():
#             if isinstance(value, dict) and value.get('name') and value.get('module'):
#                 #print('argument by name')
#                 dispatched_function['arguments'][ key ] = argument_from_context( context, value )
                
#             else:
#                 #print('argument by symbol')
#                 dispatched_function['arguments'][ key ] = value

#     #print("\n\nMANIFEST:", dispatched_function['manifest'])            
#     return dispatched_function




# class Config(object):
#     """
#     """
#     context = pipeline.Context()
    
#     ##*******************************
#     ##+~ Class methods 
#     ##*********
#     @classmethod
#     def fetch_contexts( cls, manifest ):
#         if manifest.get( "contexts" ):
#             return manifest[ 'contexts' ]
#         else:
#             return []

#     @classmethod
#     def from_manifest( cls, manifest ):
#         manifest = pipeline.configuration.read_manifest( manifest )

#         # fetch context paths from manifest
#         context_paths = cls.fetch_contexts( manifest )

#         if manifest.get( 'mode' ) or manifest.get( 'steps' ):
#             return PipelineConfig( *context_paths ).from_manifest( manifest ) 
#         elif manifest.get( 'function_object' ) or manifest.get( 'function' ):
#             return StepConfig( *context_paths ).from_manifest( manifest ) 
#         else:
#             return Config( *context_paths )


#     ##*******************************
#     ##+~ Instance Magic methods 
#     ##*********
#     def __init__( self, *paths ):
#         self.add_paths( *paths )
#         self._manifest = {}

#     def __getitem__(self, item):
#         try:
#             return getattr(self, item)
#         except AttributeError:
#             try:
#                 return self._manifest[item]
#             except KeyError:
#                 raise KeyError( self.__class__ + " object has no " + item )


#     ##*******************************
#     ##+~ Instance Properties 
#     ##*********            
#     @property
#     def contexts( self ):
#         return self.manifest.get( 'contexts' )

#     @property
#     def manifest( self ):
#         return self._manifest

#     @property
#     def paths( self ):
#         return self.context.paths
    

#     ##*******************************
#     ##+~ Instance Methods
#     ##*********
#     def add_paths( self, *paths ):
#         self.context.add_paths( *paths )
    
#     def to_manifest( self, config ):
#         return config.to_manifest()



# class PipelineConfig( Config ):
#     """
#     """
#     ##*******************************
#     ##+~ Instance Magic methods 
#     ##*********
#     def __init__( self, *paths ):
#         super().__init__( *paths )
#         self._steps = []


#     ##*******************************
#     ##+~ Instance Properties 
#     ##*********
#     @property
#     def mode( self ):
#         return self._manifest.get('mode')

#     @mode.setter
#     def mode( self, mode ):
#         if mode:
#             self._manifest['mode'] = mode

#     @property
#     def steps( self ):
#         return self._steps

#     @steps.setter
#     def steps( self, steps ):
#         if steps:
#             self._steps = steps
    

#     ##*******************************
#     ##+~ Instance Methods
#     ##*********
#     def from_manifest( self, manifest ):
#         # read manifest
#         manifest = pipeline.configuration.read_manifest( manifest )
#         self._manifest = copy_dict( manifest, ignore = ['steps'] )
        
#         # fetch mode
#         self.mode = manifest.get('mode')
        
#         # fetch context paths
#         context_paths = Config.fetch_contexts( manifest )
#         self.add_paths( *context_paths )
        
#         # decide if pipe manifest or step manifest and dispatch it accordingly
#         if manifest.get( 'steps' ):
#             for step_manifest in manifest[ 'steps' ]:
#                 step_config = StepConfig( self.context ).from_manifest( step_manifest )
#                 self.steps.append( step_config )

#         return self
    
#     def to_manifest( self ):
#         manifest = self.manifest
#         manifest[ 'steps' ] = [ step.manifest for step in self.steps ]
#         return manifest
        


# class StepConfig( Config ):
#     """
#     """
#     ##*******************************
#     ##+~ Instance Magic methods 
#     ##*********
#     def __init__( self, *paths ):
#         super().__init__( *paths )
#         self._function_object = None
#         self._arguments = None


#     ##*******************************
#     ##+~ Instance Properties 
#     ##*********
#     @property
#     def arguments( self ):
#         return self._arguments

#     @arguments.setter
#     def arguments( self, arguments ):
#         if arguments:
#             self._arguments = arguments

#     @property
#     def id( self ):
#         return self.manifest.get('id')
    
#     @property
#     def function( self ):
#         return self._manifest.get('function')

#     @function.setter
#     def function( self, function ):
#         if not self.manifest.get('function'):
#             self.manifest['function'] = function

#     @property
#     def function_object( self ):
#         return self._function_object

#     @function_object.setter
#     def function_object( self, function_object ):
#         self._function_object = function_object
    

#     ##*******************************
#     ##+~ Instance Methods
#     ##*********
#     def from_manifest( self, manifest ):
#         # read manifest
#         manifest = pipeline.configuration.read_manifest( manifest )
#         self._manifest = copy_dict( manifest, ignore = ['function_object'] )

#         # fetch context paths
#         context_paths = Config.fetch_contexts( manifest )
#         self.add_paths( *context_paths )

#         dispatched_function = step_config_from_manifest( self.context, manifest )
        
#         self.arguments = dispatched_function.get( 'arguments' )
#         self.function = dispatched_function['manifest'].get( 'function' )
#         self.function_object = dispatched_function.get( 'function_object' )
#         return self

#     def to_manifest( self ):
#         return self.manifest



#####***~~-
##################*~{ TESTING }~*##################***~~-
#####***~~-

alternative_context = pipeline.Context("../../nlp-package-1.0.0")
config = pipeline.Config( "../../nlp-package-1.0.0" )

print("\n\n### UNTYPED CONFIG CONSTRUCTOR FROM JSON ###")
untyped_config = pipeline.Config.from_manifest( pipeline_config_json )
print("CLASS:", untyped_config.__class__)
print("PATHS:", untyped_config.paths)


print("\n\n### PIPELINE CONFIG ###")
pipe_config = pipeline.PipelineConfig().from_manifest( pipeline_config_dict )
print("CLASS:", pipe_config.__class__)
print("PATHS:", pipe_config.paths)
print("MANIFEST:", pipe_config.manifest)
print("CONTEXT PATHS:", pipe_config.contexts)
print("STEPS:", pipe_config.steps)

print("\n\n### STEP CONFIG ###")
step_config = pipeline.StepConfig().from_manifest( pipeline_config_dict['steps'][3] )
print("CLASS:", step_config.__class__)
print("PATHS:", step_config.paths)
print("MANIFEST:", step_config.manifest)
print("CONTEXT PATHS:", step_config.contexts)

print("\n\n### STEP DIRECT CONFIG ###")
direct_step_config = pipeline.Config.from_manifest({
    "id": "join-the-bag-of-words",
    "function_object": return_a_str_instead_of_a_python_list,
    "arguments" : {
        "stopwords" : {
            "name" : "stopwords",
            "module" : "nlp.languages.german"
        }
    },
    "contexts" : ["../../nlp-package-1.0.0"]
})
print("CLASS:", direct_step_config.__class__)
print("PATHS:", direct_step_config.paths)
print("MANIFEST:", direct_step_config.manifest)
#print("ARGUMENTS:", direct_step_config.arguments)
print("CONTEXT PATHS:", direct_step_config.contexts)


print("\n\n#########################")
print("### MANIFEST CREATION ###")

print("\n### PIPE MANIFEST ###")
pipe_manifest = config.to_manifest( pipe_config )
pprint.pprint( pipe_manifest )

print("\n### STEP MANIFEST ###")
step_manifest = config.to_manifest( step_config )
pprint.pprint( step_manifest )


print("\n\n#####################################")
print("### CONFIG FROM MANIFEST CREATION ###")

print("\n### PIPE CONFIG FROM MANIFEST ###")
pipe_config_2 = pipeline.Config.from_manifest( pipe_manifest )
print(pipe_config_2.paths)
print(pipe_config_2.context)
pprint.pprint( pipe_config_2.steps[-1].__dict__ )

print("\n*** STEPS MANIFESTS FROM PIPE CONFIG ###")
for step in pipe_config_2[ 'steps' ]:
    print( "\n", step.to_manifest() )


print("\n\n####################################")
print("### CONFIG FROM INDIVIDUAL STEPS ###")

print("\n### PIPE CONFIG FROM SNIPPETS ###")
step_config_2 = pipeline.StepConfig()
#step_config_2.function_object = return_a_str_instead_of_a_python_list

step_config_2.id = "wtf"

step_config_2.function = { "name" : "return_a_str_instead_of_a_python_list", "module" : "__main__"}

step_config_2.arguments = {
    "stopwords" : {
        "name" : "stopwords", 
        "module" : "nlp.languages.german"
        }
}

print("return_a_str: ", step_config_2.function_object)

step_config_2.function = {
    "name" : "transformCases",
    "module" : "nlp.functions"
}

print("transformCases: ",step_config_2.function_object)

step_config_2.function = {
    "name" : "regex_cleaner",
    "module" : "nlp.functions"
}

print("regex_cleaner: ",step_config_2.function_object)


#print("\nSELF", step_config_2.__dict__)
print("\nMANIFEST", step_config_2.manifest)
print("ID", step_config_2.id)
print(step_config_2.function_object)
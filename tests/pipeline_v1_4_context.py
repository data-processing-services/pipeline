from context import pipeline

# define a test local function "map"
def map():
    "buh"
    print("map")


# load context from path
contexts = pipeline.Context("../../nlp-package-1.0.0")

# module from the context's scope
print( contexts.get_module("nlp.functions") )

# symbol from the context's module "nlp.languages.german"
print( contexts.get_symbol("stopwords", "nlp.languages.german") )

# function from the context's scope
print( contexts.get_symbol("map", "nlp.functions") )

# function from the global scope
print( contexts.get_symbol("map") )
 

# get all memebers of a module
print("\n###")
print( contexts.get_members("nlp.languages.german") )

# get all explicitly defined functions of a module
print("\n###")
print( contexts.get_functions("nlp.functions"))

# get all explicitly defined data variables of a module
print("\n###")
print( contexts.get_variables("nlp.languages.german"))
from context import pipeline
import glob
import sys
import os
import time
import json, logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)


sys.path.append("../../nlp-package-1.0.0")
import nlp

#####***~~-
##################*~{ Definitions }~*##################***~~-
#####***~~-

# defining some testdata
data = ["Uwe ist ein Mensch!", "Max ist auch ein Mensch!"]

# implementation of local function test()
def test( data, uwe ):
    s = "::: test " + uwe
    return " ".join(data) + s

# defining steps of the pipeline
steps_config = [
        {
            "id": "tokenizer-abbr",
            "function": {
                "name" : "tokenizeAbbr",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "regex" : "\w+-\w+|\w+|\S+"
            }
        },
        {
            "id": "regex-cleaner",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "[\.;]?\[\d\]|Uwe" }
        },
        {
            "id": "remove-stopwords",
            "function": {
                "name" : "removeStopwords",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "stopwords" : {
                    "name" : "stopwords",
                    "module" : "nlp.languages.german"
                }
            }
        }
    ]

# defining pipeline mode and context
pipe_config = {
    "contexts" : ["../../nlp-package-1.0.0"],
    "mode" : {
        "indices" : [ 0, 1 ],
    }   
}
pipe_config["steps"] = steps_config

# emulating a JSON config
json_config = json.dumps(pipe_config)


#####***~~-
##################*~{ Logic Testing }~*##################***~~-
#####***~~-
import pprint

# the json manifest
print("MANIFEST 1: ", json_config, "\n\n" )

# load the pipeline definition and dispatch against given context
config = pipeline.Config.from_manifest( json_config )

# show the config object
print("CONFIG 1: " )
pprint.pprint(config.__dict__ )
print("\n\n" )

print("MODE: ", config.mode, "\n\n")

pipe2 = pipeline.Pipeline()
print(pipe2.pipeline)
"""
# create a step from the config
step = pipeline.Step.from_config( config.steps[1] )
#print("STEP MANIFEST: ", step.manifest, "\n\n")


t_funct = {
    "id": "test",
    "function_object": test,
    "arguments": { "uwe" : "UWEWÄ" }
}

disp = config.dispatch_step( t_funct ) 

#pprint.pprint(disp.__dict__)

#pprint.pprint(config.steps2)

#create a pipeline from the config
pipe = pipeline.Pipeline.from_config( config )

print("CONFIG 1: " )
pprint.pprint(config.__dict__ )
print("\n\n" )

pprint.pprint(config.steps[2].manifest )


pipe.add_step( t_funct )


print(pipe.config)

# for s in pipe.pipeline:
#     print( s.config, "\n", s.manifest, "\n\n")

out = pipe.run("Hallo du Uwe da")
print(out)

"""
"""
step_main = pipe.pipeline[2]
print( step_main.base_package.__dict__ )

#pprint.pprint( step.base_package.__dict__ )
print("\n\n")
"""

"""
manifest = pipe.manifest
#print("MANIFEST: ", json.dumps(manifest, ensure_ascii=False),  "\n\n")



step1 = pipeline.Step( function_object = test, step_id ="tedtdt", arguments = {"uwe" : "Hallo"})
print( step1.manifest )


step333 = pipeline.Step.from_config( {
    "function_object": test,
    "arguments": { "uwe" : "UWEWÄ" }
} )

print(step333.manifest)
"""

"""
config_2 = pipeline.Config( manifest )
print("CONFIG 2: " )
pprint.pprint(config_2.__dict__ )
print("\n\n" )
print("CONFIG 2, STEP 3: " )
pprint.pprint(config_2.steps[2] )
print("\n\n" )
print("CONFIG 2, STEP 4: " )
pprint.pprint(config_2.steps[3].manifest )
print("\n\n" )


pipe_2 = pipeline.Pipeline.from_config( manifest )
out = pipe_2.run("Hallo du Arsch Uwe Lurch")
print(out)
"""
"""
print("\n\n")
context = pipeline.Context( *["../../nlp"] )
context.set_scope( None )

step_def = pipeline.StepDefinition({
        "id" : "uwetest",
        "function_object": test
    }, {
        "uwe" : "TestiTest"
    })

print(step_def)
print(step_def.callable)
print(step_def.arguments)

print("\n\n")

step_def = pipeline.StepDefinition({
        "id": "remove-stopwords",
        "function": {
            "name" : "removeStopwords",
            "module" : "nlp.functions"
        }
    }, 
    { "stopwords" : {
            "name" : "stopwords",
            "module" : "nlp.languages.german"
        }
    }, context)

print(step_def)
print(step_def.callable)
print(step_def.arguments)
"""
from context import pipeline
import itertools, json, random, gc 


#####***~~-
##################*~{ Definitions }~*##################***~~-
#####***~~-

# implementing some test functions
def tokenize( data ):
    return data.split(" ")

def reverse( lst ):
    lst.reverse()
    return lst

def split( data, regex ):
    return ["$"+item+"$" for item in data.split(" ")]

def append( lst, append ):
    lst.append( append )
    return lst

def addUwe( lst ):
    return ["§"+item+"Uwe" for item in lst]

def proc( string ):
    return string + " processed"


# implementing some dummy configurations
pipeline_definition = [{
        "id" : "tokenize", 
        "function_object" : tokenize
    },
    {
        "id" : "small_case", 
        "function_object" : lambda x: [ word.lower() for word in x ]
    },
    {
        "id" : "unify", 
        "function_object" : lambda x: list( set( x ) )
    },
    {
        "id" : "reverse", 
        "function_object" : reverse
    },
    {
        "id" : "remove_punctuation3", 
        "function_object" : addUwe  
    }] 

pipeline_definition_2 = [{
        "id" : "tokenize", 
        "function_object" : split,
        "arguments" : {
            'regex' : '\w+-\w+|\w+|\S+|ä'
            }
    },
    {
        "id" : "remove_punctuation3", 
        "function_object" : append,
        "arguments" : {
            'append' : "PIPE 2"
        }
    }] 

pipeline_definition_3 = [{
        "id" : "tokenize", 
        "function_object" : split,
        "arguments" : {
            'regex' : '\w+-\w+|\w+|\S+|ä'
            }
    },
    {
        "id" : "remove_punctuation3", 
        "function_object" : append,
        "arguments" : {
            'append' : "PIPE 3"
        }
    }] 

pipeline_definition_4 = [{
        "id" : "tokenize", 
        "function_object" : split,
        "arguments" : {
            'regex' : '\w+-\w+|\w+|\S+|ä'
        }
    },
    {
        "id" : "remove_punctuation3", 
        "function_object" : addUwe
    }] 

# sum up all pipeline to one list to make some random choosing awailable
pipes = [pipeline_definition, pipeline_definition_2, pipeline_definition_3, pipeline_definition_4]

# load some testing data from the filesystem
texts = pipeline.interfaces.from_dir("../data/samples/texts/*.txt")

################# Create Testsets
# Single dataset consisting of pipeline and text (LIST > DICT)
data_single = { 
    "data" : texts[1], 
    "pipeline" : {
        "contexts" : ["../../nlp"],
        # "mode_arguments" : {
        #     "indices" : [0, 1, 2, 3, 5, 6],
        #     }, 
        "steps" : pipeline_definition
        } 
    }


# Multiple datasets with one pipeline (DICT / JSON Object)
data_set_1 = { 
    "collection" : texts, 
    "pipeline" : {
        "contexts" : ["../../nlp"],
        # "mode_arguments" : {
        #     "indices" : [0, 1, 2, 3, 5, 6],
        #     }, 
        "steps" : pipeline_definition_2
        } 
    } 

# Multiple datasets with own pipelines (LIST > DICT )
data_set_2 = {
    "collection" : [ { "data" : item, "pipeline" : { "contexts" : ["../../nlp"],"steps" : random.choice(pipes)} } for item in ["Uwe ist ein dummer Hund!", "Max ist ein toller Kerl!"] ] 
    }


#####***~~-
##################*~{ Logic Testing }~*##################***~~-
#####***~~-

#### Test for single dataset with default pipeline
result = pipeline.process_resource( data_single["data"], data_single["pipeline"] )
print(result.output)

#### Test for multiple dataset with one default pipeline
result = pipeline.process_collection( data_set_1 )
for r in result:
    print(r.output)

##### Test for multiple dataset with separate pipelines
#result = pipeline.process_collection_with_separate_pipelines( data_set_2 )
result = pipeline.process_collection( data_set_2, scope = globals() )
for r in result:
    print(r.output)

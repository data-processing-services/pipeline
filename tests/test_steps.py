from context import pipeline
import sys
import gc

def test( data, regex ):
    return [item for item in data.split(" ")]

def test1( data, regex ):
    result = []
    for item in data.split(" "):
        result.append(item) 
    return result

def test2( lst, s ):
    return s + lst[1]

data = ["Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.", "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy."]


d = [{
        "id" : "tokenize", 
        "function_object" : test,
        "arguments" : {
            'regex' : '\w+-\w+|\w+|\S+|ä'
            }
    },
    {
        "id" : "remove_punctuation", 
        "function_object" : test2,
        "arguments" : {
            's' : "TEST"
        }
    },
    {
        "id" : "split", 
        "function_object" : lambda x: x.split(' ')
    }] 
    


step = pipeline.Step.from_config( d[0] )

print( "\n## Frische Instanz: \n", step.__dict__ )


print( "\n-- Ergebnis: \n", step.execute( data[1] ) )




step.duration = "Das ist ein Test"
print( "\n## Benutzte Instanz: \n", step.__dict__)

# for item in dir(step):
#     print( getattr(step, item).__dict__ )

# print( dir(step.output))
# print( dir(step).__class__)

#print( test )
#print( test1("Uwe ist doof", "zzutuzt") )

#step2 = step.clone()
#step3 = step2.clone()
#print( step )
#print( step2.run("Test ein zweiter") )
#print( step.__dict__ )
#print( step.to_json())
#print( step2.__dict__ )
#print( step2.to_json())
#print( sys.getrefcount(test2) )
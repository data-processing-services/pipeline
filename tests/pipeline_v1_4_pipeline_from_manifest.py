from context import pipeline
import glob, sys, os, time, json, inspect, pprint, logging

# sys.path.append("../../nlp-package-1.0.0")
# import nlp

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

#####***~~-
##################*~{ Test Functions }~*##################***~~-
#####***~~-

def return_a_str_instead_of_a_python_list( data, separator ):
    return separator.join(data)


#####***~~-
##################*~{ Config }~*##################***~~-
#####***~~-

pipeline_config_dict = {
    "contexts" : ["../../nlp-package-1.0.0"],
    "mode" : {
        "indices" : [0, 1, 2, 3, 4, 5, 6]
    },
    "steps" : [
        {
            "id": "tokenizer-abbr",
            "function": {
                "name" : "tokenizeAbbr",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "regex" : "\\w+-\\w+|\\w+|\\S+"
            } 
        },
        {
            "id": "regex-cleaner",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "[\\.;]?\\[\\d\\]|\\W{2}" }
        },
        {
            "id": "remove-punctuation",
            "function" : {
                "name" : "removePunctuation",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "remove-stopwords",
            "function": {
                "name" : "removeStopwords",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "stopwords" : {
                    "name" : "stopwords",
                    "module" : "nlp.languages.german"
                }
            },
            "contexts" : ["../../nlp-package-1.0.0", "../../aggrepy-1.0.0"]
        },
        {
            "id" : "lemmatise-by-cab",
            "function" : {
                "name" : "cab_lemmatiser",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "lower-cases",
            "function" : {
                "name" : "transformCases",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "clean-up",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "\\d+|\\d+[a-z]+|^\\w{2}$" }
        }
    ]
}

pipeline_config_json = json.dumps( pipeline_config_dict )

# step_config_dicts = [
#     {
#         "id": "tokenizer-abbr",
#         "function_object": nlp.functions.tokenizeAbbr,
#         "arguments" : {
#             "regex" : "\\w+-\\w+|\\w+|\\S+"
#         }
#     },
#     {
#         "id": "regex-cleaner",
#         "function_object": nlp.functions.regex_cleaner,
#         "arguments" : {
#             "regex": "[\\.;]?\\[\\d\\]|\\W{2}"
#         }
#     },
#     {
#         "id": "remove-punctuation",
#         "function_object": nlp.functions.removePunctuation
#     },
#     {
#         "id": "remove-stopwords",
#         "function_object": nlp.functions.removeStopwords,
#         "arguments" : {
#             "stopwords" : nlp.languages.german.stopwords
#         }
#     },
#     {
#         "id": "remove-stopwords-name",
#         "function_object": nlp.functions.removeStopwords,
#         "arguments" : {
#             "stopwords" : {
#                     "name" : "stopwords",
#                     "module" : "nlp.languages.german"
#                 }
#         }
#     },
#     {
#         "id": "lemmatise-by-cab",
#         "function_object": nlp.functions.cab_lemmatiser
#     },
#     {
#         "id": "lower-cases",
#         "function" : {
#             "name" : "transformCases",
#             "module" : "nlp.functions"
#         }
#     },
#     {
#         "id": "clean-up",
#         "function_object": nlp.functions.regex_cleaner,
#         "arguments" : {
#             "regex": "\\d+|\\d+[a-z]+|^\\w{2}$"
#         }
#     },
#     {
#         "id": "join-the-bag-of-words",
#         "function_object": return_a_str_instead_of_a_python_list,
#         "arguments" : {
#             "separator" : " - "
#         }
#     }
# ]

#####***~~-
##################*~{ TESTING }~*##################***~~-
#####***~~-


print("\n\n################")
print("### PIPELINE ###")

print("\n\n### PIPELINE FROM JSON MANIFEST ###")
pipe = pipeline.Pipeline( pipeline_config_json )
print("PIPE:", pipe.__dict__)
print("MODE:", pipe.mode)
print("STEPS:", pipe.pipeline)
for step in pipe.pipeline:
    print("\nCONFIG:", step )
print("CONFIG:", pipe.config )
print("MANIFEST:")
pprint.pprint(pipe.config.to_manifest())



print("\n\n### PIPELINE FROM DICT MANIFEST ###")
pipe = pipeline.Pipeline( pipeline_config_dict )
print("PIPE:", pipe.__class__)
print("CONFIG:", pipe.config )
print("STEPS:", pipe.pipeline )
print("LEN STEPS:", len(pipe.pipeline) )
for step in pipe.pipeline:
    print("\nSTEP:", step )


print("\n\n### PIPELINE FROM EXISTING CONFIG ###")
config = pipeline.Config.from_manifest( pipeline_config_json )
pipe = pipeline.Pipeline( config )
print("PIPE:", pipe.__class__)
print("CONFIG:", pipe.config )
print("STEPS:", pipe.pipeline )
print("LEN STEPS:", len(pipe.pipeline) )
for step in pipe.pipeline:
    print("\nSTEP:", step )


# print("\n\n### ADD STEP TO PIPELINE ###")
# pipe.add_step({
#     "id": "join-the-bag-of-words-direct-add",
#     "function_object": return_a_str_instead_of_a_python_list,
#     "arguments" : {
#         "separator" : " - "
#     }
# })

# print("LEN STEPS:", len(pipe.pipeline) )
# for step in pipe.pipeline:
#     print("\nSTEP:", step )

# print("MANIFEST:")
# pprint.pprint(pipe.manifest)

# direct_add_step = pipe.pipeline[-1]
# print("\nDIRECT STEP:", direct_add_step)
# print("CONFIG", direct_add_step.config)


# print("\n\n### ADD STEP TO PIPELINE BY INIT ###")
# step_2 = pipeline.Step( 
#     step_id = "something-new",
#     function_object = return_a_str_instead_of_a_python_list,
#     arguments = {
#         "separator" : " - ",
#         "stopwords" : {
#             "name" : "stopwords",
#             "module" : "nlp.languages.german"
#         }
#     },
#     contexts = ["../../nlp-package-1.0.0"]
# )
# print("DICT: ", step_2.__dict__)
# print("ID: ",step_2.id)
# print("FUNCTION_OBJECT: ",step_2.function_object )
# print("FUNCTION: ",step_2.function )
# print("ARGUMENTS: ",step_2.arguments )
# print("CONFIG: ",step_2.config)
# print("MANIFEST: ",step_2.manifest)

# pipe.add_step( step_2 )

# print("\nLEN STEPS:", len(pipe.pipeline) )
# for step in pipe.pipeline:
#     print("\nSTEP:", step )

# print("MANIFEST:")
# pprint.pprint(pipe.manifest)
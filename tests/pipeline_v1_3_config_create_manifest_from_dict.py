from context import pipeline
import glob, sys, os, time, json, inspect, logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)


sys.path.append("../../nlp-package-1.0.0")
import nlp
import pprint


pipeline_configuration = {
    "contexts" : ["../nlp-package-0.0.1"],
    "mode" : {
        "indices" : [0, 1, 2, 3, 4, 5, 6]
    },
    "steps" : [
        {
            "id": "tokenizer-abbr",
            "function": {
                "name" : "tokenizeAbbr",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "regex" : "\w+-\w+|\w+|\S+"
            }  
        },
        {
            "id": "regex-cleaner",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "[\.;]?\[\d\]|\W{2}" }
        },
        {
            "id": "remove-punctuation",
            "function" : {
                "name" : "removePunctuation",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "remove-stopwords",
            "function": {
                "name" : "removeStopwords",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "stopwords" : {
                    "name" : "stopwords",
                    "module" : "nlp.languages.german"
                }
            }
        },
        {
            "id" : "lemmatise-by-cab",
            "function" : {
                "name" : "cab_lemmatiser",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "lower-cases",
            "function" : {
                "name" : "transformCases",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "clean-up",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "\\d+|\\d+[a-z]+|^\\w{2}$" }
        }
    ]
}

json_config = json.dumps( pipeline_configuration )


def return_a_str_instead_of_a_python_list( data, separator ):
    return separator.join(data)

#####***~~-
##################*~{ Definitions }~*##################***~~-
#####***~~-



pipe_config = pipeline.PipelineConfig()

print("CONFIG:", pipe_config.__dict__, "\n\n")
print("CONTEXT:", pipe_config.context, "\n\n")

pipe_config.from_manifest( json_config )

print("CONFIG:", pipe_config.__dict__, "\n\n")
print("CONTEXT:", pipe_config.context, "\n\n")
print("STEPS:", pipe_config.steps, "\n\n")

print("##################\n\n")

# step_manifest_dict = step_defs[4]
# step_config = pipeline.StepConfig()

# print("CONFIG:", step_config.__dict__, "\n\n")
# print("CONTEXT:", step_config.context, "\n\n")

# step_config.from_manifest( step_manifest_dict )

# print("CONFIG:", step_config.__dict__, "\n\n")


## Direct Pipeline load
# pipe = pipeline.Pipeline()

# print("CONFIG:", pipe.config.__dict__, "\n\n")

# pipe.from_manifest( json_config )

# print("CONFIG:", pipe.config.__dict__, "\n\n")

# print("PIPE:", pipe.__dict__, "\n\n")

# print("PIPE-STEPS:", pipe.pipeline, "\n\n")

# print("MANIFEST:")
# pprint.pprint(pipe.manifest)
# print("\n\n")

# print("MANIFEST-TYPE:", type(pipe.manifest))



## Pipeline from manifest
#pipe = pipeline.Pipeline( json_config )
# pipe = pipeline.Pipeline( pipeline_configuration )

# print("PIPE CONFIG:", pipe.config.__dict__, "\n\n")

# print("PIPE:", pipe.__dict__, "\n\n")

# print("PIPE-STEPS:", pipe.pipeline, "\n\n")

# print("MANIFEST:")
# pprint.pprint(pipe.manifest)
# print("\n\n")

# print("MANIFEST-TYPE:", type(pipe.manifest))


# output = pipe.run(
#     data = "Das ist ein kleiner Test",
#     mode_arguments = {
#     "indices" : [0, 1, 2, 3, 5, 6]
# })


# step_3 = pipe.pipeline[3]
# print(step_3.__dict__)
# print(step_3.config)
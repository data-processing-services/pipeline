from context import pipeline
import itertools
import json


def tokenize( data ):
    return data.split(" ")

def rev( lst ):
    lst.reverse()
    return lst

pipeline_definition = [{
        "id" : "tokenize", 
        "function_object" : tokenize
    },
    {
        "id" : "small_case", 
        "function_object" : lambda x: [ word.lower() for word in x ]
    },
    {
        "id" : "unify", 
        "function_object" : lambda x: list( set( x ) )
    },
    {
        "id" : "reverse", 
        "function_object" : rev
    },
    {
        "id" : "reverse_back",
        "function_object" : rev
    }] 



sample = [{ "data":text, "pipeline":pipeline_definition} for text in pipeline.interfaces.from_dir("../data/*.txt")]

print( sample )
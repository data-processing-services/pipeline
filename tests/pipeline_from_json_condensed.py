from context import pipeline
import json, logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

#####***~~-
##################*~{ Definitions }~*##################***~~-
#####***~~-

data = ["Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.", "Uwe ist ein dummer Hund!", "Max ist ein toller Kerl."]

def test_append( data ):
    data.append("TEST_APPEND")
    return data
    
steps_config = [
    {
        "id": "tokenizer-abbr",
        "function": {
            "name" : "tokenizeAbbr",
            "module" : "nlp.functions"
        },
        "arguments" : {
            "regex" : "\w+-\w+|\w+|\S+"
        }
    },
    {
        "id": "regex-cleaner",
        "function": {
            "name" : "regex_cleaner",
            "module" : "nlp.functions"
        },
        "arguments": { "regex": "[\.;]?\[\d\]|Uwe" }
    },
    {
        "id": "remove-punctuation",
        "function" : {
            "name" : "removePunctuation",
            "module" : "nlp.functions"
        }
    },
    {
        "id": "remove-stopwords",
        "function": {
            "name" : "removeStopwords",
            "module" : "nlp.functions"
        },
        "arguments" : {
            "stopwords" : {
                "name" : "stopwords",
                "module" : "nlp.languages.german"
            }
        }
    },
    {
        "id": "lower-cases",
        "function" : {
            "name" : "transformCases",
            "module" : "nlp.functions"
        }
    }
]

# defining pipeline mode and context
pipe_config = {
    "contexts" : ["../../nlp-package-1.0.0"],
    "mode" : {
        "indices" : [0, 1, 2, 3, 5]
    },
    "steps" : steps_config
}


#####***~~-
##################*~{ Logic Testing }~*##################***~~-
#####***~~-

# initialise Pipeline and load the pipeline configuration
pipe = pipeline.Pipeline( pipe_config )

pipe.add_step({
    "function_object" : test_append
}, 2)

# execute the pipeline by config
out = pipe.run( data = data[1] )


# ... and see the result:
print(out)

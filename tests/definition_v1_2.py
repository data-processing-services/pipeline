from context import pipeline
import glob
import sys
import os
import time
import json

#####***~~-
##################*~{ Definitions }~*##################***~~-
#####***~~-

# defining some testdata
data = ["Uwe ist ein dummer Hund!", "Max ist ein toller Kerl."]

# implementation of local function test()
def test( data, uwe ):
    print("test " + uwe)
    return data

# defining steps of the pipeline
steps_config = [
        {
            "id": "tokenizer-abbr",
            "function": {
                "name" : "tokenizeAbbr",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "regex" : "\w+-\w+|\w+|\S+"
            }
        },
        {
            "id": "regex-cleaner",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "[\.;]?\[\d\]|Uwe" }
        },
        {
            "id": "remove-punctuation",
            "function" : {
                "name" : "removePunctuation",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "remove-stopwords",
            "function": {
                "name" : "removeStopwords",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "stopwords" : {
                    "name" : "stopwords",
                    "module" : "nlp.languages.german"
                }
            }
        },
        {
            "id" : "lemmatise-by-cab",
            "function" : {
                "name" : "cab_lemmatiser",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "lower-cases",
            "function" : {
                "name" : "transformCases",
                "module" : "nlp.functions"
            }
        },
        # {
        #     "id": "the-local-ending",
        #     "function_object" : test,
        #     "arguments" : { "uwe" : "buh" }
        # }
    ]

# defining pipeline mode and context
pipe_config = {
    "contexts" : ["../../nlp"],
    "mode_arguments" : {
        "indices" : [0, 1, 2, 3, 5],
    }   
}
pipe_config["steps"] = steps_config

# emulating a JSON config
json_config = json.dumps(pipe_config)


#####***~~-
##################*~{ Logic Testing }~*##################***~~-
#####***~~-


# load the pipeline definition and dispatch against given context
config = pipeline.PipelineConfig( json_config )

# show the config object
print( config.__dict__ )
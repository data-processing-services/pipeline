from context import pipeline
import glob, sys, os, time, json, inspect, pprint, logging

# sys.path.append("../../nlp-package-1.0.0")
# import nlp

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

#####***~~-
##################*~{ Test Functions }~*##################***~~-
#####***~~-

def return_a_str_instead_of_a_python_list( data, separator ):
    return separator.join(data)


#####***~~-
##################*~{ Config }~*##################***~~-
#####***~~-

pipeline_config_dict = {
    "contexts" : ["../../nlp-package-1.0.0"],
    "mode" : {
        "indices" : [0, 1, 2, 3, 4, 5, 6]
    },
    "steps" : [
        {
            "id": "tokenizer-abbr",
            "function": {
                "name" : "tokenizeAbbr",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "regex" : "\\w+-\\w+|\\w+|\\S+"
            } 
        },
        {
            "id": "regex-cleaner",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "[\\.;]?\\[\\d\\]|\\W{2}" }
        },
        {
            "id": "remove-punctuation",
            "function" : {
                "name" : "removePunctuation",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "remove-stopwords",
            "function": {
                "name" : "removeStopwords",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "stopwords" : {
                    "name" : "stopwords",
                    "module" : "nlp.languages.german"
                }
            },
            "contexts" : ["../../nlp-package-1.0.0", "../../aggrepy-1.0.0"]
        },
        {
            "id" : "lemmatise-by-cab",
            "function" : {
                "name" : "cab_lemmatiser",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "lower-cases",
            "function" : {
                "name" : "transformCases",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "clean-up",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "\\d+|\\d+[a-z]+|^\\w{2}$" }
        }
    ]
}

pipeline_config_json = json.dumps( pipeline_config_dict )

#####***~~-
##################*~{ TESTING }~*##################***~~-
#####***~~-


print("\n\n################")
print("### PIPELINE ###")

print("\n\n### PIPELINE FROM JSON MANIFEST ###")
pipe = pipeline.Pipeline( pipeline_config_json )
print("\nMANIFEST:")
pprint.pprint(pipe.config.to_manifest())

print("\nRUN THE PIPE:")
pipe.run( data = "Das ist ein kleiner, feiner aber ansich ein nichtssagender Test!" )

print("\nTHE OUTPUT:")
for step in pipe.pipeline:
    print("\nSTEP " + step.name + ": ", step.output )

print("\n##########\n\n")

print("MANIFEST:")
pprint.pprint(pipe.config.to_manifest())

print("\n\n")

print("\n\n### CLONE PIPELINE FROM OLD PIPE ###")
cloned_pipe = pipe.clone()
print("PIPE:", cloned_pipe.__dict__)





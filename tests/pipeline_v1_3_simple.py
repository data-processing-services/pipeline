from context import pipeline
import glob
import sys
import os
import time
import json

sys.path.append("../../nlp-package-1.0.0")
import nlp

#####***~~-
##################*~{ Definitions }~*##################***~~-
#####***~~-


pipeline_configuration = {
    "contexts" : ["../nlp-package-0.0.1"],
    "steps" : [
        {
            "id": "tokenizer-abbr",
            "function": {
                "name" : "tokenizeAbbr",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "regex" : "\\w+-\\w+|\\w+|\\S+"
            }  
        },
        {
            "id": "regex-cleaner",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "[\\.;]?\\[\\d\\]|\\W{2}" }
        },
        {
            "id": "remove-punctuation",
            "function" : {
                "name" : "removePunctuation",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "remove-stopwords",
            "function": {
                "name" : "removeStopwords",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "stopwords" : {
                    "name" : "stopwords",
                    "module" : "nlp.languages.german"
                }
            }
        },
        {
            "id" : "lemmatise-by-cab",
            "function" : {
                "name" : "cab_lemmatiser",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "lower-cases",
            "function" : {
                "name" : "transformCases",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "clean-up",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "\\d+|\\d+[a-z]+|^\\w{2}$" }
        }
    ]
}


#print(config.steps)
pipeline.Context("../../nlp-package-1.0.0")
for document in ["Uwe; ist ein toller Kerl", "Max ist auch ein Mensch!"]:
    
    # create a pipeline for each document of the corpus
    pipe = pipeline.Pipeline()
    
    # Way 1: just give the parsed config steps into the pipeline
    #pipe.add_steps( config.steps )
    
    # Way 2: declare all steps right away 
    pipe.add_steps([
        {
            "id": "tokenizer-abbr",
            "function_object": nlp.functions.tokenizeAbbr,
            "arguments" : {
                "regex" : "\\w+-\\w+|\\w+|\\S+"
            }
        },
        {
            "id": "regex-cleaner",
            "function_object": nlp.functions.regex_cleaner,
            "arguments" : {
                "regex": "[\\.;]?\\[\\d\\]|\\W{2}"
            }
        },
        {
            "id": "remove-punctuation",
            "function_object": nlp.functions.removePunctuation
        },
        {
            "id": "remove-stopwords",
            "function_object": nlp.functions.removeStopwords,
            "arguments" : {
                "stopwords" : nlp.languages.german.stopwords
            }
        },
        {
            "id": "lemmatise-by-cab",
            "function_object": nlp.functions.cab_lemmatiser
        },
        {
            "id": "lower-cases",
            "function_object": nlp.functions.transformCases
        },
        {
            "id": "clean-up",
            "function_object": nlp.functions.regex_cleaner,
            "arguments" : {
                "regex": "\\d+|\\d+[a-z]+|^\\w{2}$"
            }
        }
    ])
    

    # for step in pipe.pipeline:
    #     print(step.id)

    output = pipe.run(
        data = document,
        mode_arguments = {
        "indices" : [0, 1, 2, 3, 5, 6]
    })

    print(output)

    



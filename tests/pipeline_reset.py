from context import pipeline
import json, logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

#####***~~-
##################*~{ Definitions }~*##################***~~-
#####***~~-

data = ["Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.", "Uwe ist ein dummer Hund!", "Max ist ein toller Kerl."]


steps_config = [
    {
        "id": "tokenizer-abbr",
        "function": {
            "name" : "tokenizeAbbr",
            "module" : "nlp.functions"
        },
        "arguments" : {
            "regex" : "\w+-\w+|\w+|\S+"
        }
    },
    {
        "id": "regex-cleaner",
        "function": {
            "name" : "regex_cleaner",
            "module" : "nlp.functions"
        },
        "arguments": { "regex": "[\.;]?\[\d\]|Uwe" }
    },
    {
        "id": "remove-punctuation",
        "function" : {
            "name" : "removePunctuation",
            "module" : "nlp.functions"
        }
    },
    {
        "id": "remove-stopwords",
        "function": {
            "name" : "removeStopwords",
            "module" : "nlp.functions"
        },
        "arguments" : {
            "stopwords" : {
                "name" : "stopwords",
                "module" : "nlp.languages.german"
            }
        }
    },
    {
        "id": "lower-cases",
        "function" : {
            "name" : "transformCases",
            "module" : "nlp.functions"
        }
    }
]

# defining pipeline mode and context
pipe_config = {
    "contexts" : ["../../nlp-package-1.0.0"],
    "mode" : {
        "stop_with" : 3
    }
}
pipe_config["steps"] = steps_config

# emulating a JSON config
json_config = json.dumps(pipe_config)


#####***~~-
##################*~{ Logic Testing }~*##################***~~-
#####***~~-

# load the pipeline definition and dispatch against given context
config = pipeline.PipelineConfig().from_manifest( json_config )

# initialise Pipeline for single dataset and import configuration
pipe = pipeline.Pipeline( config )

out = []
# execute the pipeline by config
out.append( pipe.run( data = data[1] ) )

print(len(pipe.pipeline))
# ... and see the result:
for step in pipe.pipeline:
    print("\n", step )

# reset the pipeline
print("\n### Flushed Pipeline")
print(len(pipe.pipeline))
pipe.flush()
print(len(pipe.pipeline))

# remove step at index 3
pipe.remove_step(3)

print(len(pipe.pipeline))

# ... and execute the pipeline again
out.append( pipe.run( data = data[1] ) )

# show me again
print("\n\n")
for step in pipe.pipeline:
    print("\n", step )

# ... and print me to JSON
print(pipe.to_json())


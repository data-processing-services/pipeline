<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="xhtml:div[@class = 'section']|xhtml:div[@class='half-title']">
        <section>
            <xsl:apply-templates />
        </section>
    </xsl:template>
    
    <xsl:template match="xhtml:div[@class ='p']|xhtml:div[@class='note']|xhtml:div[@class='center-aligned']">
        <xsl:apply-templates />    
    </xsl:template>
    
    <xsl:template match="xhtml:ul">
        <xsl:apply-templates />  
    </xsl:template>
    
    <xsl:template match="xhtml:li">
        <xsl:text>&#13;*</xsl:text><xsl:apply-templates />  
    </xsl:template>
    
    <xsl:template match="xhtml:span[@class ='hi']|xhtml:span[@class ='ref']">
        <xsl:apply-templates />  
    </xsl:template>
    
    <xsl:template match="xhtml:h2">
        <xsl:apply-templates />  
    </xsl:template>
    
    <xsl:template match="a[@class ='comp-error']">
        <xsl:apply-templates />  
    </xsl:template>
    
    <xsl:template match="xhtml:a[@class ='text-link']" />
    <xsl:template match="i|br" />
    <xsl:template match="div[tokenize(@class, ' ') = ('modal')]" />
    <xsl:template match="xhtml:span[@class ='pb']|xhtml:span[@class ='foreign']" />
    <xsl:template match="xhtml:span[@class ='text-anchor']" />
    
    
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*" />
        </xsl:copy>
    </xsl:template>
    
    <!--<xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>-->
    
</xsl:stylesheet>
 21. Alles, was ein künftiger Lehrer der Religion in Absicht auf Kenntnisse zu thun
        hat, vereinigt sich in drei Hauptbeschäftigungen. Er muß sie auf die rechte Art zu sammlen,
        sie anzuordnen, und für Andere anzuwenden wissen. Um sich den nöthigen Vorrath zu einer
        eigenen wohlgegründeten Kenntniß und Ueberzeugung von der Religion zu verschaffen, wird er
        vor allen Dingen nach Kenntniß der Natur überhaupt zu streben, und besonders, bei seiner
        Bestimmung zum Lehrer der Religion, sich zu bemühen haben, über Gott, so weit es der
        endliche Verstand vermag, und über die geistige Natur des Menschen richtig denken zu lernen,
        weil ohne diese Kenntniß, welche die Philosophie darreicht, weder eine recht überzeugende
        Erkenntniß von dem Verhältniß zwischen Gott und den Menschen, womit sich die Religion
        beschäftigt, erhalten, noch ein richtiger Gebrauch der Vernunft bei solchen Untersuchungen
        gemacht werden kann. 
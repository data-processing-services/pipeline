 2. Sofern die Religion ein Gegenstand der Erkenntniß ist, kann man bei ihr, wie bei
        allen andern Gegenständen, einen Unterschied zwischen einer gemeinen und einer
        philosophischen Kenntniß derselben machen. Letztere findet nur da statt, wo eine Sache im
        vollständigen Zusammenhange mit andern, wie sie der Grund oder die Folge derselben ist,
        oder, mit andern Worten, wenn sie wissenschaftlich erkannt wird. Sie ist in dem Grade
        vollkommener, je mit mehrern Dingen man sie so verbunden denkt, und je mehrere solcher
        Verbindungen zwischen denselben eingesehen werden. Zusammenhang wird hier nicht von jeder
        Verbindung genommen, als welche eben so wie die Vorstellung dieser Verbindung, zufällig und
        willkührlich seyn kann. Nur dann ist eine Erkenntniß philosophisch, wenn man einsieht, wie
        etwas von dem Andern Grund oder Folge ist, oder wenn man das Eine aus dem Andern erklären
        kann. 
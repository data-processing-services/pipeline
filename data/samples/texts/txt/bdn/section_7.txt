 6. Doch wie – wenn Gelehrsamkeit für die Religion gefährlich wäre? – Das ist sie gewiß
        nicht; und wer dies gleichwohl meint, macht sich entweder von Gelehrsamkeit, oder Religion,
        oder von dem, was gefährlich ist, falsche Begriffe. Ohne Wegräumung dieses dreifachen
        Mißverstandes wird man wider und für die Unschuld der Gelehrsamkeit mit gleichem Glück
        streiten, und die Sache wird unverglichen bleiben . 
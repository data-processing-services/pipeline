 12. „Aber man glaubt um so weniger, je mehr man weiß; und Gelehrsamkeit ist eine
        reiche Quelle von Zweifeln!“ – Aber wer viel glaubt, wird auch viel betrogen; dagegen
        sichert demnach nichts besser, als daß man Vieles und daß man es gut wisse; also setzt uns
        wieder Gelehrsamkeit in den Stand, zu wissen, wo man glauben dürfe oder nicht? – Der
        Gelehrte zweifelt allerdings mehr wie der Ungelehrte. Aber Zweifel sind nicht immer
        schädlich; sie sind ein kräftiger Antrieb zur Untersuchung, wobei man immer gewinnt; sie
        sind sogar das einzige natürliche Mittel, von Vorurtheilen und Irrthümern zurückzukommen. –
        Und in dem Maaße, wie man in der Gelehrsamkeit wächst, nehmen auch die Kenntnisse zu, um den
        Ungrund schädlicher Zweifel einzusehen, und es wächst die Fertigkeit, sie aufzulösen; denn
        Zweifel entstehen aus Unwissenheit, und werden nur schädlich, wenn man mit ihnen nicht
        umzugehen weiß. 
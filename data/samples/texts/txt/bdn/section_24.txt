 23. So würde auch eine pragmatische Kenntniß der Geschichte überhaupt, und besonders
        der Veränderungen, die mit der Religion und der darauf gegründeten Kirche vorgegangen sind,
        außer dem schon erwähnten Nutzen, einen mächtigen Eindruck von dem so weisen Gange der
        göttlichen Fürsehung geben, der zur Erweckung der Aufmerksamkeit auf die Religion und ihren
        unaussprechlichen Werth sowohl, als auf die ganze Gesinnung gegen Gott so unentbehrlich ist.
        Sie würde den großen Einfluß der gebrauchten oder vernachlässigten Vorerkenntnisse bei der
        Religion und dem Christenthum, die seligen Folgen einer durch bescheidenen und regelmäßigen
        Gebrauch der Vernunft und der heiligen Schrift aufgeklärten Religion und ihrer
        gewissenhaften Befolgung, so wie die traurigen Folgen des Gegentheils , einleuchtend machen,
        und dadurch kräftig zu jenem ermuntern, und vor diesem warnen. Sie würde auch zeigen, wie
        weit man in der gründlichen und heilsamen Erkenntniß der Religion vorwärts oder
        zurückgekommen sei, und dadurch zu erkennen geben, was man von früheren, auf Religion
        Beziehung habenden Vorarbeiten, benutzen, verbessern oder wegräumen müsse. 
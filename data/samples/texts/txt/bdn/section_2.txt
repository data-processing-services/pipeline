 1. Wenn die Bestimmung des Menschen und das höchste Ziel seiner Wünsche, wahre und
        dauerhafte Glückseligkeit, nicht auf dieses Erdenleben eingeschränkt ist; wenn er, als ein
        vernünftiges Wesen, dieses Ziel anders nicht erreichen kann, als durch Weisheit und Tugend;
        wenn die Religion beide lehrt, unterhält, und dazu die kräftigste Ermunterung giebt, ja ohne
        sie, Weisheit, nicht wahre Weisheit, Tugend, nicht beständige Tugend seyn kann: so giebt es
        für den edeln Geist des Menschen keine würdigere Beschäftigung, als das Bestreben, die
        Religion aufs überzeugendste kennen zu lernen und aufs willigste auszuüben. 
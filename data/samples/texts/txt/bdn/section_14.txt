 13. „Gleichwohl lehrt Erfahrung und Geschichte, daß es eben Gelehrte waren, die
        Irrthümer aufbrachten, die die Religion von ihrer Einfalt zurückführten, die sie ihrer
        Geheimnisse zu berauben suchten!“ – Wenn dies Gelehrte gethan haben sollten, so müßte erst,
        ehe man sie verdammen wollte, das ausgemacht werden, was oben §. 8. erinnert ist. Aber gewiß
        sind jene Verderbnisse der Religion weit häufigere Folgen der Unwissenheit, des
        Mißverstandes, der Schwärmerei oder des aftergelehrten Dünkels, welchem Fehler eben die
        Gelehrsamkeit entgegenarbeitet. 
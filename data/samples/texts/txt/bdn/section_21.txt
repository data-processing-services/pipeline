 20. Diese Pflichten und die hohen Zwecke, wozu der geistliche Stand da ist, einst
        würdig zu erfüllen, dazu gehört die gewissenhafteste Prüfung, ob man überhaupt dazu fähig
        und fest entschlossen sei, so wie ein ununterbrochenes Bestreben, immer fähiger und
        geschickter zu werden. Eine solche Vorbereitung erfordert, daß man wisse: 1) welche Arten
        von Kenntnissen nützlich oder unentbehrlich sind, um sich zu einem künftigen Lehrer der
        Religion zu bilden; 2) welche Fähigkeiten nöthig sind, um diese zu erlangen und auf das
        nützlichste zu Anderer Besten anzuwenden; 3) welche Hülfsmittel und Uebungen dazu dienen. 
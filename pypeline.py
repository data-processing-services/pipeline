#!/usr/bin/env python
import pipeline as pipe
import json

# @click.command()
# @click.argument('target')
# @click.option("--config", "-c", required=True, type=str, help="Location to save pypelines output.")
# @click.option("--out", "-o", type=str, help="Location to save pypelines output.")
# def main(target, config, out):
#     """Pypeline CLI"""
#     config = "../data/samples/configs/sample_config.json"
#     pipe_config = {
#     "contexts" : ["../nlp-package"],
#     "steps" : [
#         {
#             "id": "tokenizer-abbr",
#             "function": {
#                 "name" : "tokenizeAbbr",
#                 "module" : "nlp.functions",
#                 "arguments" : {
#                     "regex" : "\w+-\w+|\w+|\S+"
#                 }
                
#             }
#         },
#         {
#             "id": "regex-cleaner",
#             "function": {
#                 "name" : "regex_cleaner",
#                 "module" : "nlp.functions",
#                 "arguments": { "regex": "[\.;]?\[\d\]|Uwe" }
#             }
#         },
#         {
#             "id": "remove-punctuation",
#             "function" : {
#                 "name" : "removePunctuation",
#                 "module" : "nlp.functions"
#             }
#         },
#         {
#             "id": "remove-stopwords",
#             "function": {
#                 "name" : "removeStopwords",
#                 "module" : "nlp.functions",
#                 "arguments" : {
#                     "stopwords" : {
#                         "name" : "stopwords",
#                         "module" : "nlp.languages.german"
#                     }
#                 }
#             }
#         },
#         {
#             "id" : "lemmatise-by-cab",
#             "function" : {
#                 "name" : "cab_lemmatiser",
#                 "module" : "nlp.functions"
#             }
#         },
#         {
#             "id": "lower-cases",
#             "function" : {
#                 "name" : "transformCases",
#                 "module" : "nlp.functions"
#             }
#         }
#     ]
# }
#     print("TARGET:", target )
#     print("CONFIG:", config )
#     json_config = json.loads(pipe.read_file( config ))

#     pipeline = pipe.Pipeline( pipe_config )
#     print( pipeline.__dict__ )
    
#     if out:
#         print("save pipelines output to ", out)


if __name__ == "__main__":
    pipe.interfaces.CLI()
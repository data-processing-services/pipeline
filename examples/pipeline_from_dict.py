#!/usr/bin/env python3

""" 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

NOTE:
This Example won't work if you haven't put the nlp-package in the same directory where your pipeline folder is. If you don't have the nlp-package ... no problem: Just define your own module or package of functions and use it instead. All you need to do then is to modify the functions name and the module in the Steps configuration

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
""" 


from context import pipeline
import json

# declaration of some dull textual data
data = ["Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.", "Uwe ist gerne Äpfel direkt vom Baum!", "Max ist ein toller Kerl.", "Fischers Fritze fischt frische Fische; frische Fische fischt Fischers Fritze!", "Blaukraut bleibt Blaukraut und Brautkleid bleibt Brautkleid"]


# configure the Pipeline with context and separate steps
# - Mode: a pipeline has diferent modes of execution. If your are interested in than initialise a Pipeline and look up it's __doc__
# - Context: an item in context represents a location where the modules or packages defining the functions you referenced in the steps configuration above resides. If you don't specify it, the Pipeline can not resolve your functions
# Note: Make shure that your packages have a proper __init__.py 
pipe_config = {
    "contexts" : ["../../nlp-package-1.0.0"],
    "steps" : [
        {
            "id": "tokenizer-abbr",
            "function": {
                "name" : "tokenizeAbbr",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "regex" : "\w+-\w+|\w+|\S+"
            }
        },
        {
            "id": "regex-cleaner",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "[\.;]?\[\d\]|Uwe" }
        },
        {
            "id": "remove-punctuation",
            "function" : {
                "name" : "removePunctuation",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "remove-stopwords",
            "function": {
                "name" : "removeStopwords",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "stopwords" : {
                    "name" : "stopwords",
                    "module" : "nlp.languages.german"
                }
            }
        },
        {
            "id" : "lemmatise-by-cab",
            "function" : {
                "name" : "cab_lemmatiser",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "lower-cases",
            "function" : {
                "name" : "transformCases",
                "module" : "nlp.functions"
            }
        }
    ]
}


# initialise Pipeline for single dataset and import configuration
pipe = pipeline.Pipeline( pipe_config )


# ... in case you didn't already look it up, here is the __doc__ of the pipeline runner so you get an idea of on how many modes you can run a pipeline
print( "This is how the pipeline runner is ment to be used:\n\n", pipe.run.__doc__ , "\n---------- # ----------\n")


# now let's execute the pipeline to step 4 (at index 3)
reslut = pipe.run( 
    data = data[1], 
    mode = {
        "stop_with" : 3
    })


# ... and see the result:
print( "The Pipelines result:\n\n", reslut , "\n---------- # ----------\n")

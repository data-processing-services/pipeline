#!/usr/bin/env python3

from context import pipeline
import json, pprint


# declaration of some dull variables
punctuation = ".’'()[]{}<>:,‒–—―…!.«»-‐?‘’“”„;/⁄␠·&@*\\•^¤¢$€£¥₩₪†‡°¡¿¬#№%‰‱¶′§~¨_|¦⁂☞∴‽※"


# declaration of some dull nlp function
def remove_punctuation ( data, punct_char ):
    pc = [c for c in punct_char]
    if isinstance(data, str):
        return "".join([char for char in data if char not in pc])
    elif isinstance(data, list):
        filtered = []
        for token in data:
            cleared = "".join([char for char in token if char not in pc])
            if cleared != "":
                filtered.append( cleared )
        return filtered


# initialise a single Pipeline
pipe = pipeline.Pipeline()


# add a step to the pipe: (1) Tokenize the Text and use a lambda (anonymous) function
pipe.add_step({
    "id" : "tokenize",
    "function_object" : lambda x: x.split(' ')
})


# add another step to the pipe: (2) Remove all punctuation characters from the data
pipe.add_step({
    "id" : "remove-punctuation", 
    "function_object" : remove_punctuation,
    "arguments" : {
        'punct_char' : punctuation
    }
})


# before we execute the pipeline lets have a look what is under the hood
print( "The nonexecuted Pipeline Instance under the Hood:\n\n", pipe , "\n---------- # ----------\n")


# execute the pipeline ...
result = pipe.run( data = "Das ist ein simpler Test, der verdeutlichen soll, dass die Pipeline auch ohne große JSON Config verwendet werden kann. Einfach Funktionen definieren, Pipeline initialisieren, Schritte hinzufügen und ausführen. Fertig!" )


# ... and show me what happened to the pipeline ...
print( "And this is how the executed Pipeline Instance looks like:\n\n", pipe , "\n---------- # ----------\n")


# ... and finaly print the output
print( "The Pipelines result:\n\n", result , "\n---------- # ----------\n")
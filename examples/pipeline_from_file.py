#!/usr/bin/env python3

""" 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

NOTE:
This Example won't work if you haven't put the nlp-package in the same directory where your pipeline folder is. If you don't have the nlp-package ... no problem: Just define your own module or package of functions and use it instead. All you need to do then is to modify the functions name and the module in the Steps configuration

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
""" 


from context import pipeline
import json

# declaration of some dull textual data
data = ["Uwe ist gerne Äpfel direkt vom Baum!", "Max ist ein toller Kerl.", "Fischers Fritze fischt frische Fische; frische Fische fischt Fischers Fritze!", "Blaukraut bleibt Blaukraut ... und Brautkleid bleibt Brautkleid!"]


# load a json config from filesystem
json_config = json.loads(pipeline.read_file("../data/samples/configs/bdn.pipeconf.json"))

print(json_config)

# initialise Pipeline for single dataset and import configuration
pipe = pipeline.Pipeline( json_config )


# ... in case you didn't already look it up, here is the __doc__ of the pipeline runner so you get an idea of on how many modes you can run a pipeline
print( "This is how the pipeline runner is ment to be used:\n\n", pipe.run.__doc__ , "\n---------- # ----------\n")


# now let's execute the pipeline and ignore step 4
test_data = data[3]
result = pipe.run( 
    data = test_data, 
    mode = {
        "indices" : [0, 1, 2, 3, 5]
    })


# ... and see the result:
print( "The Pipelines result for \"" + test_data + "\":\n\n", result , "\n---------- # ----------\n")


# ... and if you like you may export the pipeline to JSOn:
print( "The Pipelines as JSON:\n\n", pipe.to_json({"sort_keys" : True, "indent" : 4}) , "\n---------- # ----------\n")
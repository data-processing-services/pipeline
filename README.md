# Pipeline (v.1.0.4)

A pythonic pipeline to convert textual data via defined conversion steps. The Pipeline is configurable using python dictionaries or a JSON Manifest.

## Installation

Download this package from [GitLab](https://gitlab.gwdg.de/data-processing-services/pipeline)

Use python to install pipeline. python version 3 is required:

```bash
python3 setup.py install
```

## Usage

To use Pipeline you need to import the Module pipeline

```python
import pipeline
```

### Basic Concepts of the Pipeline

Pipeline consists of mainly four core concepts that are worth to introduce.

#### Pipeline

The Pipeline itself is nothing but a batch processing tool that converts a given input by running to a defined set of Steps.

In a simple Usecase a Pipeline can be defined and simply excecuted, that means every Step is run sequentially.

You may also configure a mode the Pipeline should use to excecute. which can become handy if you like to test certain conversion routines:

```python
# instantiate a Pipeline Step with excecution mode 'indices', aka. the steps that shall be excecuted. Others will be ignored
pipe = pipeline.Pipeline({
    "mode" : {
        "indices" : [0, 4, 5, 6]
    }
)
```

To get an overview of all modes you may have a look into the documentation of `Pipeline.run`:

```python
"""
Executes all defined steps of the pipeline and returns the processed data.

Args:
    data                : an arbitrary data object that is going to be processed
    mode (dict)         : (optional) a dictionary holding arguments to interface the different pipeline modes. If it is missing the pipeline will be run from start to end.
        indices (list)  : (optional) the indices of steps going to be processed
        step_id (str)   : (optional) the id of a step going to be processed
        start_with (int): (optional) the index of a step to start the pipelines execution at
        stop_with (int) : (optional) the index of a step to stop the pipelines execution with

Returns:
    object : an arbitrary data object representing the output of the pipelines processing

Examples:
    Pipeline.run( data ) #executes the whole pipeline
    Pipeline.run( data, {"start_with" : 0, "stop_with" : 2} ) # executes the first three Steps at index 0, 1 and 2
    Pipeline.run( data, {"stop_with" : 1} ) # executes the first two Steps at index 0 and 1
    Pipeline.run( data, {"indices" : [0, 1, 2, 3, 5, 7]} ) # executes just the Steps given as indices and ignores 4 and 6
    Pipeline.run( data, {"step_id" : "regex-cleaner"} ) # executes a step by it's id

Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
Date: 2019-10-29
Modified:
    2019-11-10 (v1.1) - added interface to use different run modes
    2019-11-11 (v1.2) - changed run mode arguments from keyword arguments to a dictionary object for better handling
"""
```

#### Steps

A Pipeline consists of individual steps that can be processed in a given order. Steps can be thougt of as a Wrapper around python functions:

```python
# instantiate a Pipeline Step
pipe_step = pipeline.Step(
    function_object = someFunction
    arguments = {
        "some_argument" : ["1", "2", "some"]
    }
)
```

#### Context

A Context defines the scope of symbols that can be used by the Pipeline. As we will see later the context is used to resolve functions, variables or any other symbol dynamically from Modules or Packages.

#### Manifest

A Manifest is a JSON file that contains configuration parameters to load a Pipeline.

A Pipeline Manifest may contain following parameters:

- __"contexts"__  : (list of paths) Paths to Modules / Packages that provide functions or variables to use in the pipeline. These paths are optional and they should be used if you import functions from modules dynamically
- __"mode"__      : (dictionary of mode arguments) The execution mode of the pipeline. The mode is optional as long as you just want to run a pipeline from start to end.
- __"steps"__     : (list of Step manifests) Inidividual Step definitions

A Step Manifest may contain following parameters:

- __"contexts"__          : (list of paths) Paths to Modules / Packages that provide functions or variables to use in the pipeline. If these context paths where already provided by the pipeline Manifest there is no need to provide them with the Step Manifests again.
- __"id"__                : (string, OPTIONAL) The ID of a Step
- __"function_object"__   : (pointer of a python symbol) The Step's python callable
- __"arguments"__         : (dictionary of key, value pairs) Arguments given to the callable as parameters
  - _key_                 : (string) The Name of the argument to pass to the callable
  - _value_               : (object) An abitrary python data object as the value of an argument

Besides the "function_object" you can provide a function property using Module Manifest Syntax (MMS). The same applies to argument declarations:

- __"function"__          : (dictionary of key, value pairs) The function declaration using MMS
  - __"name"__            : (string) The name of a function
  - __"module"__          : (string) The name of the Module where the function is declared. You can use pythons module dot notation.
- __"argument"__          : (dictionary of key, value pairs) The argument declaration using MMS
  - __"name"__            : (string) The name of a variable
  - __"module"__          : (string) The name of the Module where the variable is declared. You can use pythons module dot notation.

### Simple use within your python scripts

```python
import pipeline

# declaration of some dull variables
punctuation = ".’'()[]{}<>:,‒–—―…!.«»-‐?‘’“”„;/⁄␠·&@*\\•^¤¢$€£¥₩₪†‡°¡¿¬#№%‰‱¶′§~¨_|¦⁂☞∴‽※"

# declaration of some dull function
def remove_punctuation ( data, punct_char ):
    pc = [c for c in punct_char]
    if isinstance(data, str):
        return "".join([char for char in data if char not in pc])
    elif isinstance(data, list):
        filtered = []
        for token in data:
            cleared = "".join([char for char in token if char not in pc])
            if cleared != "":
                filtered.append( cleared )
        return filtered

# instantiate a Pipeline
pipe = pipeline.Pipeline()

# add a lambda function to your pipeline. The only thing you should keep in mind is that your function actually returns something.
pipe.add_step({
    "id" : "tokenize",
    "function_object" : lambda x: x.split(' ')
})

# add a function pointer to your pipeline and provide arguments to your function. Note that the function (1) needs a first positional data argument (you don't need to include into the arguemnts property) which provides the input data to every function and (2) returns the converted output. The pipeline cares to forward the data throughout the different steps for you.
pipe.add_step({
    "id" : "remove-punctuation",
    "function_object" : remove_punctuation,
    "arguments" : {
        'punct_char' : punctuation
    }
})

# execute the pipeline ...
result = pipe.run( data = "Das ist ein simpler Test, der verdeutlichen soll, dass die Pipeline ohne JSON Manifest verwendet werden kann. Einfach Funktionen definieren, Pipeline initialisieren, Schritte hinzufügen und ausführen. Fertig!" )

# ... and show me what happened to the pipeline ...
print( pipe )


# ... and finaly have a look into the output
print( result )

```

### Module Imports and the Module Manifest Syntax (MMS)

The pipeline can handle Module and Package imports. You can provide a path of a python Package on your system and the pipeline handles the import to load functions, variables or any other symbol from there.

To describe symbols from modules by string a special syntax is used, the Module Manifest Syntax (MMS) to provide the function's name and module as strings:

```python
# define a step the direct way
step_manifest_without_MMS = {
    "id" : "transform_cases",
    "function_object" : nlp.functions.transformCases
}

# define a step using MMS providing the function's name and module as strings
step_manifest_using_MMS = {
    "id" : "transform_cases",
    "function" : {
        "name" : "transformCases",
        "module" : "nlp.functions"
    }
}

# define a step using MMS for the function as well as for arguments
step_manifest_using_MMS_with_arguments = {
    "id": "remove-stopwords-german",
    "function": {
        "name" : "removeStopwords",
        "module" : "nlp.functions"
    },
    "arguments" : {
        "separator" : " - ",
        "stopwords" : {
            "name" : "stopwords",
            "module" : "nlp.languages.german"
        }
    }
}
```

This workflow is used if you don't want to import Modules / Packages directly and fixed into your application but to load components from there dynamically (That beeing said we just encountered the cornerstone of how to configure a Pipeline using a JSON Manifest). If a context path is provided the Pipeline registeres it to PYTHON_PATH and loads it if a function from there is required:

```python
# instantiate a Pipeline and provide context paths of modules providing functions you want to use so Pipeline can regeister it
pipe = pipeline.Pipeline({"contexts" : ["../../nlp-package-1.0.0", "../../aggrepy-1.0.0"]})

# add a step using the Module Manifest Syntax (MMS) so the pipeline can resolve the function from the module. If the Module "nlp" is already imported within your application there is no need to import it.
pipe.add_step({
    "id" : "transform_cases",
    "function" : {
        "name" : "transformCases",
        "module" : "nlp.functions"
    }
})

# you even may provide context paths directly in a step's Manifest
pipe.add_step({
    "id" : "transform_cases",
    "function" : {
        "name" : "transformCases",
        "module" : "some.functions"
    },
    "contexts" : ["some/path/of/a/module"]
})

```

The Class which handles the dynamical imports of Modules / Packages and resolves symbols by name is the Context Class defined in `pipeline/context.py`:

```python
# load context from path
context = pipeline.Context("../../nlp-package-1.0.0")

# module from the context's scope
nlp_module = contexts.get_module("nlp.functions")

# symbol from the context's module "nlp.languages.german"
nlp_german_stopwords = contexts.get_symbol("stopwords", "nlp.languages.german")
```

This means you may use the Context Class on its own to get any symbol from a Module by string.

Anayway, consider the case that you already imported a module into your application to draw functions etc. from. In that case you obviously DO NOT need to provide context paths, neither using direct Step declaration nore using MMS:

```python
import nlp

# instantiate a Pipeline without any context paths.
pipe = pipeline.Pipeline()

# define a step the direct way without MMS
pipe.add_step({
    "id" : "transform_cases_direct",
    "function_object" : nlp.functions.transformCases
})

# define a step using MMS
pipe.add_step({
    "id" : "transform_cases_mms",
    "function" : {
        "name" : "transformCases",
        "module" : "nlp.functions"
    }
})

```

### Complex Usecase using a pipeline configuration manifest

Now that you are familiar with the basic concepts of the Pipeline and the MMS we may try to configure a Pipeline using a JSON Manifest.

Asume we have this JSON Manifest (lets call it "manifest.json") to create our Pipeline from:

```json
{
    "contexts" : ["../../nlp-package-1.0.0"],
    "mode" : {
        "indices" : [0, 1, 2, 3, 4, 5, 6]
    },
    "steps" : [
        {
            "id": "tokenizer-abbr",
            "function": {
                "name" : "tokenizeAbbr",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "regex" : "\\w+-\\w+|\\w+|\\S+"
            }
        },
        {
            "id": "regex-cleaner",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "[\\.;]?\\[\\d\\]|\\W{2}" }
        },
        {
            "id": "remove-punctuation",
            "function" : {
                "name" : "removePunctuation",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "remove-stopwords",
            "function": {
                "name" : "removeStopwords",
                "module" : "nlp.functions"
            },
            "arguments" : {
                "stopwords" : {
                    "name" : "stopwords",
                    "module" : "nlp.languages.german"
                }
            }
        },
        {
            "id" : "lemmatise-by-cab",
            "function" : {
                "name" : "cab_lemmatiser",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "lower-cases",
            "function" : {
                "name" : "transformCases",
                "module" : "nlp.functions"
            }
        },
        {
            "id": "clean-up",
            "function": {
                "name" : "regex_cleaner",
                "module" : "nlp.functions"
            },
            "arguments": { "regex": "\\d+|\\d+[a-z]+|^\\w{2}$" }
        }
    ]
}
```

Now you can load this manifest and feed it to your Pipeline:

```python
# read the manifest.json from your file system. You may use the pipeline's filereader if you like.
json_manifest = pipeline.read_file("manifest.json")

# instantiate a Pipeline and provide the manifest. The Pipeline does not care about JSON or Python dictionaries; it simply converts the Manifest automatically.
pipe = pipeline.Pipeline( json_manifest )
```

Done. If you like to execute the Pipeline just do so:

```python
# execute the pipeline ...
result = pipe.run( data = "Das ist ein simpler Test, der verdeutlichen soll, dass die Pipeline ohne JSON Manifest verwendet werden kann. Einfach Funktionen definieren, Pipeline initialisieren, Schritte hinzufügen und ausführen. Fertig!" )
```

### More Examples

You may have a look into `examples`. There you find a selection of some sample scripts exemplifying the useage of the Pipeline somewhat deeper.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[AGPL-3.0-or-later](LICENSE)

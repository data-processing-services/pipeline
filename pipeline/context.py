"""
context.py
----------

This module implements the Context class, a helper class that provides a way to dynamicaly load modules, classes and functions on runtime by string from a defined path.

Date    : 2019-11-08
Version : 1.1 (2020-04-24)
Author  : Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019

Modified:
    2020-04-24  : version 1.1 refactoring of Context Class. Now can be used for resolving symbols by name and module as string in general.
"""

import importlib, inspect, logging, os, re, sys


## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ##
## ++       Module Functions
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ##
def append_path_to_pythonpath( path ):
    """
    Appends a path to sys.path if it does not already exists
    
    This functions is needed to append a path to sys.path so modules and packages can be found there.

    Args:
        path (str): the path to a package appended to PYTHONPATH
    
    Returns:
        str : the path
                
    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-11-08
    """
    if path not in sys.path:
        sys.path.append( path )
        logging.info('appended path "' + path + '" to sys.path')
    return path


def import_module_from_path( path, name = None ):
    name = re.sub(".py", "", path.split("/")[-1] ) if name == None else name
    spec = importlib.util.spec_from_file_location( name, path )
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    logging.info('loaded module"' + name + '" from path "' + path + '"')
    return module


def ismagicdescriptor( tupl ):
    return (tupl[0].startswith("__") and tupl[0].endswith("__"))


def iscallable( tupl ):
    return callable(tupl[1])


def ismodule( tupl ):
    try:
        if "__file__" in dir(tupl[1]):
            return True
        return False
    except:
        return False


def isvariable( tupl ):
    return not ( ismagicdescriptor(tupl) or iscallable(tupl) or ismodule(tupl))


def main_script():
    """
    returns the main script
        
    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
    Date: 2020-04-14
    """
    import __main__
    return __main__


def get_module( main, name ):
    modules = name.split('.')
    if len(modules) > 1:
        base = modules[0]
        #print( base)
        if hasattr( main, base ):
            module = getattr( main, base )
            if inspect.ismodule( module ):
                return get_module( module, ".".join(modules[1:]) )
            elif callable( module ):
                return main
        else:
            raise ModuleNotFoundError("No module named '" + base + "'")
    else:
        if hasattr( main, name ):
            module = getattr( main, name )
            if inspect.ismodule( module ):
                return module
            elif callable( module ):
                return main
        else:
            raise ModuleNotFoundError("No module named '" + name + "'")


def get_funct( name ):
    """
    PROTOTYPE
    """
    main = main_script()
    if hasattr( main, name ):
        return getattr( main, name )
    else:
        split = name.split(".")
        module = __import__(".".join(split[:-1]), fromlist=[''])
        if hasattr( module, split[-1] ):
            return getattr( module, split[-1] )
        else:
            None



## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ##
## ++       Module Classes
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ##
class Context:
    """
    Initialises a Context Instance to define module and package contexts for functions, classes or any other symbols to look up for the pipeline

    This functions is needed to gather all symbols by strings from the environment of a pipeline

    Args:
        *paths (str): a set of paths as strings
        
    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-11-08
    """

    ##*******************************
    ##+~ Magic methods 
    ##*********
    def __init__( self, *paths ):
        self.paths = []
        self.add_paths( *paths )
        
    
    ##*******************************
    ##+~ Class Properties
    ##*********
    @property
    def main( self ):
        """
        GETTER: return the main module calling this function

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-04-15
        """
        return main_script()


    ##*******************************
    ##+~ Class Methods
    ##*********
    def add_paths( self, *paths ):
        """
        Adds a path to the context object

        This functions is needed to dynamically add paths to the context where modules or packages are placed to be loaded

        Args:
            path (str): the path to a package appended to PYTHONPATH
            
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-08
        """
        for path in paths:
            if isinstance(path, str) and not path in self.paths:
                self.paths.append( append_path_to_pythonpath(path) )



    def get_module( self, module_name, module_path = None ):
        """
        Gets a module from the defined context dynamically

        This functions is needed to dynamically import modules by string

        Args:
            module_name (str)   : a module's name
            module_path (str)   : the path to a package appended to PYTHONPATH
            scope (dict)        : (optional) a python dictionary containing an additional symbols table like globals() or locals() to draw functions, variables etc. from. This may be used to resolve symbols by string

        Returns:
            module: a module
                    
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-08
        """
        # append module_path to python-path is the is one
        if module_path:
            self.add_paths( module_path )

        # if modules_name is already imported take it from the main script
        if module_name == "__main__":
            return self.main
        else:
            try:
                module = get_module( self.main , module_name )
                logging.info('loading module "' + module_name + '" from __main__ "' + self.main.__file__ + '"')
                return module
            except ModuleNotFoundError:
                module = self.import_module( module_name )
                return module
    

    def get_symbol( self, symbol_name, module_name = None, module_path = None ):
        """
        Loads a symbol dynamically from a module or from the global scope of the script executed.

        This functions is needed to dynamically import symbols by name

        Args:
            symbol_name (str)   : the name of the symbol to load
            module_name (str)   : (optional) the module's name the symbol is declared in
            module_path (str)   : (optional) the path to a package appended to PYTHONPATH

        Returns:
            object: an arbitrary object
                    
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-08
        """
        if not module_name:
            logging.info('loading symbol "' + symbol_name + '" from __main__ "' + self.main.__file__ + '"')
            return getattr( self.main, symbol_name )
        elif module_name:
            module = self.get_module( module_name, module_path )
            logging.info('loading symbol "' + symbol_name + '" from module "' + module_name + '"')
            return getattr(module, symbol_name)

    
    def get_members(self, module_name, module_path = None, predicate = None):
        module = self.get_module( module_name, module_path )
        return inspect.getmembers( module ) if predicate == None else inspect.getmembers( module, predicate )
    

    def get_functions(self, module_name, module_path = None, predicate = None):
        return self.get_members( module_name, module_path, predicate=inspect.isfunction )
    

    def get_variables(self, module_name, module_path = None, predicate = None):
        module = self.get_module( module_name, module_path )
        return [ item for item in inspect.getmembers( module ) if isvariable(item) ]

    def import_module( self, name ):
        module = __import__( name, fromlist=[''] )
        logging.info('importing module "' + name + '" from "' + module.__file__ + '"')
        return module
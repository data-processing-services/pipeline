#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Uwe Sikora <sikora@sub.uni-goettingen.de>
# Licensed under the ...

"""
serialiser.py
-------

This module implements the serialiser class to serialise output in different forms.

Date    : 2020-02-27
Version : 1.0
Author  : Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
"""

import copy, inspect, json, sys, time, pickle, logging


class Serialiser:
    """
    A Serialiser provides methods to plot data in variouse formats
    
    Args:
        data (dict)   : (optional) a python dictionary containing Step configurations to initialise the Pipeline with

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2020-02-27
    """
    def __init__( self , data ):
        self._data = data

    def encode( self, **kwargs ):
        return self._data.encode( **kwargs ) 

    def raw( self ):
        return self._data

    def json( self, **kwargs ):
        if not kwargs.get('ensure_ascii'):
            kwargs['ensure_ascii'] = False 
        return json.dumps( self._data, **kwargs )

    def text( self, separator = " ", encode = None ):
        string = separator.join( self._data )
        if encode:
            string = string.encode( **encode )
        
        return string
    
    def serialise( self, format, **kwargs):
        return getattr(self, format)( **kwargs )
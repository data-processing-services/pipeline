"""
corpus.py
-------------

This module implements a corpus class to interface with pipeline and gensim package

Date    : 2019-11-25
Version : 1.0
Author  : Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
"""

from .pipeline import Pipeline
from .interfaces import pickle_load, pickle_save, read_file
import io, glob, pickle, logging, json


class Corpus( object ):

    @classmethod
    def load( cls, path ):
        """
        Factory method to load a Corpus object from pickle binary at a given path 

        Args:
            path (str): pyth to a python pickle binary 
                
        Returns:
            Corpus : a Corpus instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-21
        """
        return pickle_load( path, cls )

    def __init__(self, metadata ):
        self._metadata = metadata
        self._resources = []

    ##******************************************************************************
    ##+~ Magic Methods ~+##
    def __iter__( self ):
        return iter( self.resources ) 

    def __repr__(self):
        return f"<{self.__class__.__name__} { self._metadata }>"
    
    def __getitem__( self, index ):
        return self.resources[ index ]
    
    ##******************************************************************************
    ##+~ Attributes, Getter and Setter ~+##
    @property
    def resources( self ):
        return self._resources

    @resources.setter
    def resources( self, resources ):
        self._resources = [ self.add_resource( resource ) for resource in resources ]
    
    ##******************************************************************************
    ##+~ Methods ~+##
    def add_resource( self, resource ):
        if isinstance( resource, Resource ):
            self.resources.append( resource )
        elif isinstance( resource, dict ) and resource.get("metadata") and resource.get("data"):
            self.resources.append( Resource(resource["metadata"], resource["data"]) )
    
    def save( self, path , form = None ):
        if form and form == "text":
            f = open( path, "a")
            for item in self.resources:
                f.write( item.to_json() + "\n" )
            f.close()
        else:
            return pickle_save( self, path, "corpus" )
"""
errors.py
---------

This module implements Pipeline Error classes to handle exceptions more package specific

Date    : 2019-11-11
Version : 1.0
Author  : Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
"""

import sys 


class Error(Exception):
    """
    ...
    
    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-10-29
    """
    def __init__( self, message ):
        super().__init__(message)
        self.error = sys.exc_info()


class ProcessingError(Error):
    """
    Raise when the Processing of a step went wrong        
    
    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-10-29
    """
    def __init__( self, message ):
        super().__init__(message)


class PipelineError(Error):
    """
    Raise when the Pipeline broke     
    
    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-10-29
    """
    def __init__( self, message ):
        super().__init__(message)
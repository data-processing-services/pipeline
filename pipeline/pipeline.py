#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Uwe Sikora <sikora@sub.uni-goettingen.de>
# Licensed under the ...

"""
pipeline.py
-------

This module implements the main classes Pipeline and Step to process data in a configurable workflow.

Date    : 2019-10-29
Version : 1.4 (2020-04-24)
Author  : Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019

Modified:
    2020-04-16  : version 1.3 using Context Instances
    2020-04-24  : version 1.4 refactoring of Config Class and integration into the pipeline. Also updated the from_manifest and to_manifest logic.
"""

import copy, hashlib, inspect, itertools, json, logging, pickle, sys, time, uuid
from .errors import Error, ProcessingError, PipelineError
from .configuration import PipelineConfig, Config, StepConfig, base_package
from .context import Context

class Pipeline:
    """
    A Pipeline containing Processing Steps. A Pipeline can be used to execute to process data according to defined steps.
    The Definition can be a JSON string or a python list
    
    Args:
        config (dict)   : (optional) a python dictionary containing Step configurations to initialise the Pipeline with
        scope (dict)    : (optional) a python dictionary containing an additional symbols table like globals() or locals() to draw functions, variables etc. from. This may be used to resolve symbols by string

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-10-29
    """
    ##*******************************
    ##+~ Magic methods 
    ##*********
    def __init__( self , config = None ):
        self.config = config
        self.pipeline = self.config.steps
        self._broken = False
    
    def __repr__(self):
        return f"<{self.__class__.__name__} { self.to_json() }>"


    @classmethod
    def load( cls, path ):
        """
        Factory method to load a Pipeline object from pickle binary at a given path 

        Args:
            path (str): pyth to a python pickle binary 
                
        Returns:
            Pipeline : a Pipeline instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-21
        """
        pipe = pickle.load( open(path, 'rb') )
        if isinstance(pipe, Pipeline):
            logging.info("Loaded Pipeline from '" + path + "' binary")
            return pipe
        else:
            logging.error("Binary '" + path + "' is not a Pipeline!")


    ##*******************************
    ##+~ Properties
    ##*********
    @property
    def broken( self ):
        """
        Indicates wether the pipeline instance was executed without errors or broke during execution

        Returns:
            boolean : status of the pipeline instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-13
        """
        return self._broken

    @broken.setter
    def broken( self, boolean ):
        """
        Sets Pipeline.broken attribute

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-13
        """
        self._broken = boolean if isinstance(boolean, bool) else True
    

    @property
    def config( self ):
        """
        The Pipeline configuration object

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2020-04-23
        """
        return self._config

    @config.setter
    def config( self, config ):
        """
        Sets the Pipeline configuration object. If no manifest is provided a new config is created by reading the manifest.

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2020-04-23
        """
        if config:
            if isinstance(config, PipelineConfig):
                logging.info( "Pipeline-Configuration: Loading existing Configuration" )
                self._config = config
            else:    
                logging.info( "Pipeline-Configuration: Creating new Configuration from Manifest" )
                self._config = PipelineConfig().from_manifest( config )
            
        else:
            logging.info( "Pipeline-Configuration: Creating raw Configuration" )
            self._config = PipelineConfig()


    @property
    def duration( self ):
        """
        The pipeline's time to execute.

        Returns:
            float : the time 

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        candidates = []
        for step in self.pipeline:
            try:
                if step.duration:
                    candidates.append(step)
            except:
                pass
        
        result = sum(step.duration for step in candidates)
        return result


    @property
    def elapsed_time( self ):
        """
        The pipeline's time to execute.
        
        Returns:
            str : the time encoded as hh:mm:ss 

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        return time.strftime( "%H:%M:%S", time.gmtime( self.duration ) )


    @property
    def errors( self ):
        """
        Processing Errors that may happen during execution.
        
        Returns:
            list | None : a list containing all errors if there are any

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        errors = [ step for step in self.pipeline if hasattr(step, "error") ] 
        if len(errors) == 0:
            return None
        else:
            return errors 


    @property
    def packages( self ):
        packages = [step.base_package.__path__ for step in self.pipeline if step.module != "__main__" ]
        return list( set( itertools.chain( *packages ) ) )


    @property
    def manifest( self ):
        """
        creates a manifest dictionary of the Pipeline that can be used as config

        Returns:
            dict : the manifest of a Pipeline
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-04-14
        """ 
        return { 
            'contexts' : self.packages,
            'mode'  : self.mode,
            'steps' : [ step.manifest for step in self.pipeline ]
        }


    @property
    def mode( self ):
        """
        The execution mode of the pipeline if any specified

        Returns:
            dict : the mode properties of the pipeline instance
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-04-23
        """ 
        return self.config.mode if self.config else None

    @mode.setter
    def mode( self, mode_dict ):
        """
        SETTER: The execution mode of the pipeline. The mode is set in the pipelines config
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-04-23
        """ 
        self.config.mode = mode_dict
        

    @property
    def output( self ):
        """
        A Pipeline's result taken from the the last processing step.
        
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        index = -1
        if self.broken == True:
            return None
        else: 
            try:
                while self.pipeline[index].output == None:
                    #print(self.pipeline[index].id)
                    index-=1
                return  self.pipeline[index].output
                #return copy.deepcopy( self.pipeline[index].output )
            except IndexError:
                return None
    

    @property
    def pipeline( self ):
        """
        Gets the Pipeline.pipeline attribute representing a list of all Steps of the Pipeline instance

        Returns:
            list : Steps of the Pipeline instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-13
        """
        return self._pipeline

    @pipeline.setter
    def pipeline( self, steps ):
        """
        Sets Pipeline.pipeline attribute

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-13
        """

        #check if Pipeline.pipeline exists so steps can be appended
        if not hasattr(self, "_pipeline"):
            self._pipeline = []

        self.add_steps( steps ) 


    ##*******************************
    ##+~ Methods
    ##*********
    def add_step( self, definition, insert = None ):
        """
        Adds a Processing Step to the Pipeline.pipeline attribut either at the end (default) or to a position specified by an index

        Args:
            definition (dict)   : a Step Definition Dict containing "name" (string), "function_object" (function object), "arguments" ( a list containing key:value pairs ).
            insert (int)        : (optional) an index to insert the Step into the Pipeline.pipeline. If the index is out of the pipeline's range the Step will be added at the Pipeline's end.     
        
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified:
            2019-11-13 (v1.1) : implemented insert
        """

        # determin if definition is already a Step or a StepConfig. 
        # If not create a new StepConfig from the given Manifest. 
        try:
            if isinstance( definition, Step ):
                logging.debug( "Pipeline.add_step: take preconfigured Step" )
                step = definition
            elif isinstance( definition, StepConfig ):
                logging.debug( "Pipeline.add_step: create Step from Configuration" )
                step = Step( config = definition ) 
            elif isinstance( definition, dict ):
                logging.debug( "Pipeline.add_step: create Step from Manifest" )
                step = Step.from_manifest( definition )
            else:
                raise TypeError

            if insert and isinstance(insert, int) and insert < len(self.pipeline): 
                logging.debug( "Pipeline.add_step: insert Step at position " + str(insert) )
                self.pipeline.insert( insert, step )
            else:
                logging.debug( "Pipeline.add_step: append Step at last position " + str(len(self.pipeline)+1) )
                self.pipeline.append( step )
                
            logging.debug("Pipeline.add_step: step is added to pipeline")
        except:
            raise Exception( "ERROR adding step because of config logic" )

        
    def add_steps( self, steps ):
        """
        WARNING: Duplicate to steps_from_config()
        Defines the steps of the pipeline from a python dict and returns the Pipeline.pipeline property (list). 

        Args:
            steps (list): a list of Step Definition dictionaries containing "name" (string), "function_object" (function object), "arguments" ( a list containing key:value pairs ) 
        
        Returns:
            list    : all declared processing steps of the pipeline 

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified:
            2019-11-12 (v1.1) : using Pipeline.add_step instead of writing self.pipeline directly
        """
        for step in steps:
            self.add_step( step ) 
        return self.pipeline


    def clone( self ):
        """
        Clones the Pipeline Instance and returns a fresh Instance incl. all processing steps.
        
        Returns:
            Pipeline    : A Pipeline instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Note: NOT realy in USE!
        """
        cloned_pipeline = Pipeline( self.config )
        logging.info("Pipeline.clone: Cloned Pipeline from Config")
        return cloned_pipeline 


    def flush( self ):
        """
        Resets the pipeline instance.

        This method is implemented to use a pipeline instance again without residual data, errors etc. but with the same workflow
        
        Returns:
            Pipeline    : the cleaned Pipeline instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-101-12
        """
        self._pipeline = [ step.reset() for step in self.pipeline ]
        self.broken = False
        logging.info("flushed Pipeline")
        return self


    def from_manifest( self, manifest ):
        """
        Configures a Pipeline instance from a pipeline config object while providing validation using custom errors 

        Args:
            json_config (dict): a Pipeline Definition Dict containing "name" (string), "function_object" (function object), "arguments" ( a list containing key:value pairs ) 
                
        Returns:
            Pipeline : a Pipeline instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-04-14
        """
        self.config.from_manifest( manifest )
        self.pipeline = self.config.steps
        self._broken = False


    def remove_step( self, index = None ):
        """
        Adds a Processing Step to the Pipeline.pipeline attribut either at the end (default) or to a position specified by an index

        Args:
            index (int)        : (optional) an index to remove the Step from the Pipeline.pipeline index. If the index is None or out of the pipeline's range the last Step will be removed.     
        
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-13
        """
        if index and isinstance(index, int) and index < len(self.pipeline): 
            del self.pipeline[ index ]
        else:
            del self.pipeline[ -1 ]


    def save( self, path ):
        """
        Saves a pipeline instance to disk in a python pickle binary

        Args:
            path (string): a path to serialise the pipeline as pickle binary 

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-22
        """
        name = path+".pipe" if not path.split(".")[-1] == "pipe" else path
        pickle.dump( self, open( name, 'wb') )
        logging.info("Saved Pipeline to '" + name + "' as binary")  
    

    def step_result( self, index ):
        """
        Returns the result of a Pipeline Step by its index.

        Args:
            index (int): the step's index to get the result
        
        Returns:
            object    : the result of a step
        
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29    
        """
        return self.get_step_by_index( index ).output


    def to_json( self, parameter = None ):
        """
        Converts the Pipeline instance into a JSON representation
        
        Retuns:
            str : a JSON representation of the Pipeline

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        properties = [ 'elapsed_time', 'output', 'broken' ]
        features = { item : getattr(self, item) for item in properties if getattr(self, item)}
        features["pipeline"] = [ json.loads(step.to_json()) for step in self.pipeline ]
        return json.dumps( features, ensure_ascii=False, **parameter ) if parameter else json.dumps( features, ensure_ascii=False )


    ##+~ ~~ Retrieval Methods ~~
    def get_step_by_index( self, index ):
        """
        Retrieval Method to get a Processing Step by its index.
        
        Args:
            index (int): the index of a Step from the Pipeline.pipeline list

        Returns:
            Step : a Step instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        try:
            return self.pipeline[ index ]
        except IndexError:
            logging.error("IndexError at Pipeline.get_step_by_index() : index " + str(index) + " is out of range of pipeline list")


    def get_steps_by_id( self, id ):
        """
        Retrieval Method to get a Processing Step by its defined identifier.
        
        Args:
            id (str): the id of a Step from the Pipeline.pipeline list

        Returns:
            Step : a Step instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        return list(filter( lambda x: x.id == id, self.pipeline ))
    

    def get_sliced_pipeline( self, start, stop, step = None ):
        """
        Retrieval Method to get a section of a pipeline.

        Args:
            start (int): the start of the slice
            stop  (int): the stop of the slice
            step  (int): the steps of a slice
        
        Returns:
            list : a list containing a section from the pipeline
        
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        return self.pipeline[ slice(start, stop, step) ]


    ##+~ ~~ Runner Methods ~~
    def run( self, data , mode = None ):
        """
        Executes all defined steps of the pipeline and returns the processed data. 

        Args:
            data                : an arbitrary data object that is going to be processed
            mode (dict)         : (optional) a dictionary holding arguments to interface the different pipeline modes. If it is missing the pipeline will be run from start to end.
                indices (list)  : (optional) the indices of steps going to be processed
                step_id (str)   : (optional) the id of a step going to be processed
                start_with (int): (optional) the index of a step to start the pipelines execution at
                stop_with (int) : (optional) the index of a step to stop the pipelines execution with
        
        Returns:
            object : an arbitrary data object representing the output of the pipelines processing
        
        Examples:
            Pipeline.run( data ) #executes the whole pipeline
            Pipeline.run( data, {"start_with" : 0, "stop_with" : 2} ) # executes the first three Steps at index 0, 1 and 2
            Pipeline.run( data, {"stop_with" : 1} ) # executes the first two Steps at index 0 and 1 
            Pipeline.run( data, {"indices" : [0, 1, 2, 3, 5, 7]} ) # executes just the Steps given as indices and ignores 4 and 6
            Pipeline.run( data, {"step_id" : "regex-cleaner"} ) # executes a step by it's id

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified: 
            2019-11-10 (v1.1) - added interface to use different run modes
            2019-11-11 (v1.2) - changed run mode arguments from keyword arguments to a dictionary object for better handling
        """
        # set the mode arguments and take self.mode as default if there are any
        if mode:
            self.mode = mode
        elif self.mode and len(self.mode) > 0:
            mode = self.mode

        if len(self.pipeline) > 0:
            if mode:
                if mode.get("indices"):
                    return self.run_steps_by_indices( indices = mode["indices"], data = data )
                elif mode.get("step_id"):
                    return self.run_step_by_id( step_id = mode["step_id"], data = data )
                elif mode.get("start_with") or mode.get("stop_with"):
                    if not mode.get("stop_with"):
                        return self.run_section( start_with = mode["start_with"], data = data )
                    elif not mode.get("start_with"):
                        return self.run_section( start_with = 0, stop_with = mode["stop_with"], data = data )
                    else:
                        return self.run_section( start_with = mode["start_with"], stop_with = mode["stop_with"], data = data )
                else:
                    return self.run_steps( steps = self.pipeline, data = data )     
            else:
                return self.run_steps( steps = self.pipeline, data = data )
        else:
            logging.info("Pipeline.run(): This Pipeline is empty. Define Steps first usind Pipeline.add_step() method!")


    def run_step( self, step, data, add=False ):
        """
        Executes a step of the pipeline and returns the processed data. 

        Args:
            step : a Step instance or a python dict configuration of a step that is going to be executed 
            data : an arbitrary data object that is going to be processed 
        
        Returns:
            object : an arbitrary data object representing the output of a step

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified: 
            2019-11-29 (v1.1) - added instance check to method to virtualy create steps from python dictionary and excecute it
            2019-12-04 (v1.2) - added add flag to add a function to the pipeline dynamically
        """
        if not isinstance(step, dict):
            try:
                return step.execute( data )
            except ProcessingError as error:
                logging.critical("PipelineError:: Pipeline.run_step(): Pipeline crushed with step " + step.id + " and error: " + error.args[0])
                self.broken = True
        else:
            try:
                # add virtual steps to pipeline if add == True
                if add:
                    self.add_step( step )
                    step_to_process = self.pipeline[-1]
                else:
                    step_to_process = Step.from_manifest( step )
           
                return step_to_process.execute( data )
            except ProcessingError as error:
                logging.critical("PipelineError:: Pipeline.run_step() on step_dict: step " + step_to_process.id + " crushed with error: " + error.args[0])


    def run_steps( self, steps, data ):
        """
        Executes a set of steps of the pipeline and returns the processed data. 

        Args:
            steps (list): a list of Step instances going to be executed
            data        : an arbitrary data object that is going to be processed 
        
        Returns:
            object : an arbitrary data object representing the output after all given steps finished execution
        
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        for step in steps:
            # Abort the mission!
            if self.broken == True:
                break
            # ... or presume!
            data = self.run_step( step = step, data = data )
                
        return data
    

    def run_steps_by_indices( self, indices, data ):
        """
        Executes steps of the pipeline by their indices and returns the processed data. 

        Args:
            indices (list): the indices of steps going to be executed
            data          : an arbitrary data object that is going to be processed 
        
        Returns:
            object : an arbitrary data object representing the output after all steps specified by index finished execution
        
        Examplex:
            Pipeline.run_steps_by_indices( indices = [0, 1, 2, 3, 5, 10], data = data )
            Pipeline.run_steps_by_indices( indices = [3], data = data )

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified: 2019-11-09 (v1.1) - now using self.run_steps insted of implementing its for...each routine again
        """
        steps = []
        for index in indices:
            step = self.get_step_by_index( index )
            if step:
                steps.append( step )

        return self.run_steps( steps = steps, data = data )


    def run_step_by_id( self, step_id, data ):
        """
        Executes a step of the pipeline by its name and returns the processed data. 

        Args:
            step_id (str)   : the id of a step going to be executed
            data            : an arbitrary data object that is going to be processed 
        
        Returns:
            object : an arbitrary data object representing the output of a step specified by its id
        
        Examples:
            Pipeline.run_step_by_id( step_id = "regex-cleaner", data = data )

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified: 2019-11-10 (v1.1) - added error catching if there is no step with the specified id
        """
        try:
            step = self.get_steps_by_id( step_id )[0]
            return self.run_steps( steps = [ step ], data = data ) 
        except IndexError:
            logging.error("PipelineError:: Pipeline.run_step_by_id(): No step with id " + step_id)
    

    def run_section( self, data, start_with, stop_with = None ):
        """
        Executes a pipeline from a defined start step by index. 

        Args:
            data             : an arbitrary data object that is going to be processed  
            start_with (int) : the index of a step to start the pipeline's execution at
            stop_with (int)  : (optional) the index of a step to stop the pipeline's execution with. If there is no explicitly defined stop the pipeline will run to all steps starting at the defined beginning.
        
        Returns:
            object : an arbitrary data object representing the output after all steps of the specified section finished execution
        
        Examples:
            Pipeline.run_section( start_with = 0, stop_with = 2, data = data )
            Pipeline.run_section( start_with = 3, data = data )
            
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-09
        """
        stop_with = (stop_with + 1) if stop_with else len(self.pipeline)
        pipeline_section = self.pipeline[start_with:stop_with]
        return self.run_steps( pipeline_section, data )



class Step:
    """
    A Processing Step. Returns a Step Instance with .name (str), .function_object (function object), .arguments (list of key:value pairs), .output (arbitrary data) and .duration (number representing the duration of a step) properties.

    Args:
        function_object (object): (optional) the function object reference of the step
        step_id         (str)   : (optional) the ID of the Step.
        arguments       (dict)  : (optional) a dict containing the keyword arguments used by the function object.
        config          (object): (optional) a Step Configuration or a manifest
        contexts        (lst)   : (optional) a list containing paths to python modules from where functions can be loaded.
            
    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-10-29
    """
    ##*******************************
    ##+~ Magic methods ~+##
    ##*********
    def __init__( 
        self, 
        function_object = None, 
        step_id = None , 
        arguments = None, 
        config = None, 
        contexts = [] ):
        
        self.config = config
        self.contexts = contexts
        self.function_object = function_object
        self.id = step_id
        self.arguments = arguments

        self.output = None
        self.duration = None    

    def __call__( self, **arguments ):
        # set self.arguments as default ...
        _arguments = self.arguments
        # ... and update by provided arguments
        for key, value in arguments.items():
            _arguments[key] = value
        return self.function_object( **_arguments )

    def __repr__(self):
        return f'<{self.__class__.__name__} id="{ self.id }", function={ self.function }, arguments={ self.arguments }, output={ self.output }, duration={ self.duration }, doc, manifest>'


    ##*******************************
    ##+~ Class methods ~+##
    ##*********
    @classmethod
    def from_manifest( cls, manifest ):
        """
        Factory method to create a Step instance from a manifest 

        Args:
            manifest (str|dict): a Step manifest as JSON or python dict containing "name" (string), "function_object" (function object), "arguments" ( a list containing key:value pairs ) 
                
        Returns:
            Step : a Step instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified:
            2019-11-13 (v1.1) : implemented further Exception handling
            2019-11-14 (v1.2) : made a classmethod
            2020-04-24 (v1.4) : total refactoring
        """
        config = StepConfig().from_manifest( manifest )
        instance = cls(
            config = config
        )
        
        return instance


    ##*******************************
    ##+~ Instance Properties
    ##*********
    @property
    def config( self ):
        """
        The Step configuration object

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2020-04-23
        """
        return self._config
    
    @config.setter
    def config ( self, config ):
        """
        Sets the Step configuration object. If no manifest is provided a new config is created by reading the manifest.

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2020-04-23
        """
        if config:
            if isinstance( config, StepConfig ):
                logging.debug( "Step.config: Take existing config" )
                self._config = config
            else:
                logging.debug( "Step.config: Create new config from manifest" )
                self._config = StepConfig().from_manifest( config )
        else:
            logging.debug( "Step.config: Create new raw config" )
            self._config = StepConfig()


    @property
    def contexts( self ):
        """
        The context paths of the config object representing the paths of python modules
        to load functions from. This property actually passes through to the Context Class
        provided with every Config Instance.

        Returns:
            lst : a list of string representing paths of python modules
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2020-04-23
        """
        return self.config.contexts

    @contexts.setter
    def contexts( self, contexts ):
        """
        SETTER: Sets the context paths of the config object representing the paths of python modules
        to load functions from. This property actually passes through to the Context Class
        provided with every Config Instance, that is handling the modules import and symbols dispatching.

        Args:
            contexts    (lst) : a list of strings representing paths of python modules

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2020-04-23
        """
        self.config.contexts = contexts


    # OKAY
    @property
    def arguments( self ):
        """
        The actual arguments used with the Step's callable function. 
        It passes throug to the Config Class that handles the symbols dispathing.

        Returns:
            dict    : a dictionary containing all arguments as key : value pairs

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2020-04-23
        """
        return self.config.arguments

    # OKAY
    @arguments.setter
    def arguments ( self, argument_dict ):
        """
        SETTER: The actual arguments used with the Step's callable function. 
        It passes throug to the Config Class that handles the symbols dispathing. That means
        that in that very case a arguments definition contains a manifest model syntax the Config Class
        will try to resolve the manifest properties "name" and "model" into a python symbol.

        Args:
            argument_dict   (dict)  : a dictionary containing argument definitions to dispatch

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2020-04-23
        """
        if not self.config.arguments:
            self.config.arguments = argument_dict

    

    @property
    def base_package( self ):
        """
        The base package of module.

        Returns:
            module    : a the base package of the Step-callables module

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2020-04-23
        """
        return base_package( self.module )

    
    @property
    def manifest( self ):
        """
        creates a manifest dictionary of the Step that can be used as config

        Returns:
            dict : the manifest of a Step
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-04-14
        """ 
        config_manifest = self.config.manifest
        manifest_properties = ['id', 'doc']
        for prop in manifest_properties:
            if prop not in config_manifest:
                config_manifest[ prop ] = getattr( self, prop )

        return config_manifest



    @property
    def doc( self ):
        """
        Returns the functions __doc__ string.

        Returns:
            str : the doc string of a function
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        return inspect.getsource( self.function_object )


    @property
    def duration( self ):
        """
        The Step's duration after excecution.

        Returns:
            str : the duration of a Step
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        return self._duration
    
    @duration.setter
    def duration ( self, duration ):
        """
        SETTER: The Step's duration after excecution.

        Args:
            duration    (float) : the duration of a Step
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        if duration and isinstance(duration, float):
            logging.info("Step executed in " + str(duration) )
            self._duration = duration 
        elif not duration: 
            logging.debug("Step.duration was set to None since it was tried to enter not a float type!")
            self._duration = None 
            

    @property
    def error( self ):
        try:
            return self._error
        except:
            return None
    
    @error.setter
    def error ( self, error ):
        self._error = error

    @error.deleter
    def error( self ): 
        if hasattr(self, "_error"):
            del self._error


    @property
    def function( self ):
        """
        The Step's callable definition containing the manifest syntax including the callable's name and module.

        Returns:
            dict    : a dictionary containing the callable's name and module-name as strings
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified:
            2020-04-24 (v1.4) : now using Step's Config Instance 
        """
        return self.config.function

    @function.setter
    def function ( self, function_dict ):
        """
        SETTER: Sets the Step's callable definition containing the manifest syntax including the callable's name and module.
        This propety actually passes through to the Step's Config Instance, which dispatches the given manifest
        syntax into a callable. That means that Config.function sets the _function property as well as the _function_object property
        within the Config.

        Args:
            function_dict   (dict)  : a dictionary containing the callable's name and module-name as strings
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified:
            2020-04-24 (v1.4) : now using Step's Config Instance
        """
        self.config.function = function_dict


    @property
    def function_object( self ):
        """
        The Step's callable that was resolved by the Config instance

        Returns:
            callable    : the Step's callable function
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified:
            2020-04-24 (v1.4) : now using Step's Config Instance
        """
        return self.config.function_object
    
    @function_object.setter
    def function_object ( self, obj ):
        """
        SETTER: Sets the Step's callable within the Step's Config Instance.

        Args:
            obj   (callable)  : a dictionary containing the callable's name and module-name as strings
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified:
            2020-04-24 (v1.4) : now using Step's Config Instance
        """
        if obj:
            if not self.config.function_object:
                #print("function does not exist in config ... create it")
                if callable( obj ):
                    self.config.function_object = obj
                else:
                    log = "CallableError: The function object of Step " + self.id + " is not a callable!"
                    logging.error( log )
                    raise Exception( log )
        

    @property
    def id( self ):
        """
        The Step's ID.

        Returns:
            str    : the Step's ID as string
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        return self._id
    
    # OKAY
    @id.setter
    def id ( self, step_id ):
        """
        SETTER: Sets the Step's ID. 
        If a ID is provided the Step's Config Instance will be updated. If not the ID is taken from the Config Instance.

        Args:
            step_id   (str|None)  : a string representing the ID or None
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        Modified:
            2020-04-24 (v1.4) : now populating Step's Config Instance
        """
        if step_id and isinstance( step_id, str ):
            self.config.id = step_id
            self._id = step_id
        else:
            self._id = self.config.id


    @property
    def module( self ):
        """
        Returns the functions __module__ string.

        Returns:
            str : the module string of a function
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-13
        """
        return self.function_object.__module__


    @property
    def name( self ):
        """
        Returns the functions __name__ string.

        Returns:
            str : the name string of a function
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-13
        """
        #print(type(self.function_object.__name__))
        if self.function_object:
            return self.function_object.__name__
        else:
            return ""


    @property
    def output( self ):
        """
        The Step's output after execution.

        Returns:
            object : an abitrary python data structure reprenting the Step's conversion output.
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-13
        """
        return self._output
    
    @output.setter
    def output ( self, data ):
        """
        SETTER: Sets the Step's output after execution.

        Args:
            data    (object) : an abitrary python data structure reprenting the Step's conversion output.
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-11-13
        """
        self._output = data
    

    ##*******************************
    ##+~ Methods
    ##*********
    def clone( self ):
        """
        Clones itself and returns the clone
        
        Returns:
            Step : a new instance of Step

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        return Step(  
            function_object = self.function_object, 
            step_id = self.id,
            arguments = self.arguments
            )
    

    def execute( self, data ):
        """
        Executes the step on specified data and returns the processed data. 

        Args:
            data : an arbitrary object of data that is going to be processed.
        
        Returns:
            object : the output of the step's processing of the given data
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        
        try:
            # try and run the step and stop the time running
            start = time.time()
            self.output = self.function_object( data, **self.arguments ) if self.arguments else self.function_object( data )
            self.duration = time.time() - start
            # we need to deepcopy the output to get two seperate objects
            return copy.deepcopy(self.output)
        except:
            # if the step fails raise an ProcessingError no matter what. Document it in the step. We may catch it in the pipeline
            error = sys.exc_info()
            self._error = str(error[0]) + ': ' + str(error[1])
            log = "ProcessingError@" + self.id +": Step failed: " + self._error
            logging.error( log )
            raise ProcessingError( log )


    def reset( self ):
        """
        Resets the Step instance

        This Method is used to clean of any residuals from a step to use it again

        Returns:
            str : a JSON String

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        self.output = None
        self.duration = None
        if hasattr(self, "error"):
            del self.error
        return self
    

    def to_json( self ):
        """
        Returns a JSON representation of the Processing Step.
                
        Returns:
            str : a JSON String

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        properties = [ 'id', 'duration', 'output', 'doc' ]
        features = { item : getattr(self, item) for item in properties if getattr(self, item)}
        features["function"] = self.function
        return json.dumps( features, ensure_ascii=False )


"""
interfaces.py
-------------

This module implements helper functions to interface with the modules of the pipeline package

Date    : 2019-11-11
Version : 1.0
Author  : Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
"""

from .pipeline import Pipeline
from .serialiser import Serialiser
from .configuration import Config
import click, io, glob, pickle, logging, json, os


###################################################################
###     INTERFACE Module Functions
#

def process_from_config( data, pipeline_config, return_pipe = True ):
    """
    Interface function to process a single dataset via a configured pipeline 

    Args:
        data (object)                    : the data to process
        pipeline_config (Config) : a Config instance
        return_pipe (boolean)            : (optional) If True the pipeline instance is returned, if False the pipeline's output

    Returns:
        object|Pipeline : the output of a pipeline or the pipeline itself

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-11-11
    """
    pipe = Pipeline( pipeline_config )
    if return_pipe:
        pipe.run( data = data )
        return pipe
    else:
        return pipe.run( data = data )


def process_resource( data, config, return_pipe = True ):
    """
    Interface function to process a single dataset via a configured pipeline 

    Args:
        data (object)           : the data to process
        config (str|dict)       : a configuration as string or dictionary
        return_pipe (boolean)   : (optional) If True the pipeline instance is returned, if False the pipeline's output

    Returns:
        object|Pipeline : the output of a pipeline or the pipeline itself

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-11-11
    """
    config = Config.from_manifest( config )
    return process_from_config( data, config, return_pipe )


def process_collection( collection, return_pipe = True ):
    """
    Interface function to process a single dataset via a configured pipeline 

    Args:
        collection (list)       : the datasets to process
        return_pipe (boolean)   : (optional) If True the pipeline instance is returned, if False the pipeline's output

    Returns:
        object|Pipeline : the output of a pipeline or the pipeline itself

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-11-11
    """
    default_config = Config( collection["pipeline"] ) if collection.get("pipeline") else None
    output = []
    for resource in collection["collection"]:
        if not isinstance(resource, str):
            config = Config( resource["pipeline"] ) if resource.get("pipeline") else default_config
            output.append( 
                process_from_config( resource["data"], config, return_pipe ) 
            )
        else:
            output.append( 
                process_from_config( resource, default_config, return_pipe ) 
            )
    return output
    

def read_dir( path ):
    """
    Interface function to read a directory

    Args:
        path (str) : the path to read in

    Returns:
        list : all resources in the path

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-10-29
    """
    return glob.glob( path )


def read_file( path, encoding='utf-8', lines=False ):
    """
    Interface function to read a file from a path

    Args:
        path (str)      : the path to read in
        encoding (str)  : (optional; default: utf-8) the encoding of the file.
        lines (boolean) : (optional, default: False) If True readin lines if False read the file as a whole.

    Returns:
        str|list : the file as string or as list per lines

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-10-29
    """
    try:
        with open(path, mode='r', encoding=encoding ) as resource:
            if lines:
                return resource.readlines()
            else:
                return resource.read()
    except UnicodeDecodeError as unicode_error:
        print(unicode_error)
        with io.open(path, mode='r', encoding='ISO-8859-1') as resource:
            if lines:
                return resource.readlines()
            else:
                return resource.read()


def from_dir( path, encoding="utf-8", lines=False ):
    """
    Interface function to read files from a path

    Args:
        path (str)      : the path to read in
        encoding (str)  : (optional; default: utf-8) the encoding of the file.
        lines (boolean) : (optional, default: False) If True readin lines if False read the file as a whole.

    Returns:
        list : files from a path

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-10-29
    """
    resources = read_dir( path )
    return [ read_file( resource, encoding=encoding, lines=lines ) for resource in resources ]


def pickle_load( path, cls=None ):
    """
    loads an object from pickle binary at the given path 

    Args:
        path (str): pyth to a python pickle binary 
            
    Returns:
        object : a python object

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-11-21
    """
    obj = pickle.load( open(path, 'rb') )
    if cls:
        if isinstance(obj, cls):
            logging.debug("Succesfully loaded " + cls.__name__ + " at '" + path + "'!")
            return obj
        else:
            logging.error("Binary '" + path + "' is not a " + cls.__name__ + "!")
    else:
        logging.info("Loading " + obj.__class__.__name__ + " without typecheck.")
        logging.debug("Succesfully loaded " + obj.__class__.__name__ + " at '" + path + "'!")
        return obj


def pickle_save( obj, path, extension = "" ):
    """
    saves an object to a pickle binary to the given path 

    Args:
        path (str): pyth to a python pickle binary 
            
    Returns:
        object : a python object

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2019-11-21
    """
    name = path + "." + extension if not path.split(".")[-1] == extension else path
    pickle.dump( obj, open( name, 'wb') )
    logging.debug("Saved " + obj.__class__.__name__ + " to '" + name + "' as binary")


def save_file( path, name, data ):
    if not os.path.isdir( path ):
        create_dir( path )
    f = open( os.path.join(path, name), "w+")
    f.write( data )
    f.close()
    
def split_path( path ):
    return os.path.split( path )

def file_name_from_path( path ):
    split = split_path( path )
    tail = split[-1] if split[-1] != "" else split[0].split('/')[-1]
    return tail

def create_dir( path ):
    try:
        os.makedirs( path, exist_ok = True ) 
    except OSError:
        print ("Creation of the directory %s failed" % path)
    else:
        return path

@click.command()
@click.argument("target")
@click.argument("config")
# @click.option("--file", "-f", required=True, type=str, help="File to convert.")
# @click.option("--dir", "-d", required=True, type=str, help="Directory keeping files to convert.")
# @click.option("--config", "-c", required=False, type=str, help="Location to save pypelines output.")
@click.option("--extension", "-e", default="", type=str, help="Filetypes to convert.")
@click.option("--modules", "-m", multiple=True, help="Filetypes to convert.")
@click.option("--out", "-o", type=str, help="Location to save pypelines output.")
@click.option("--of", "-f", type=str, default="text", help="Format of the output.")

def CLI(target, config, extension, modules, out, of):
    """Pypeline CLI"""
    # print("TARGET:", target )
    # print("CONFIG:", config )

    config_dict = json.loads( read_file( config ) )
    
    if modules:
        contexts = config_dict["contexts"] if config_dict.get("contexts") else []
        for module in modules:
            contexts.append( module )
        config_dict["contexts"] = contexts

    def _convert_resource( path, out ):
        name = os.path.splitext( file_name_from_path( path ) )[0]
        print(name)
        resource = Resource( {"name" : name + ".conv.txt"} )
        resource.from_path( path )
        result = resource.pipeline( config = config_dict )
        resource.data = Serialiser( result ).serialise( format = of )
        print(resource.data)
        if out:
            print("save pipelines output to ", out)
            resource.save( out )

    if os.path.isdir( target ):
        paths = read_dir(target + "/*" + extension)
        for path in paths:
            _convert_resource( path, out )

    elif os.path.isfile( target ):
        _convert_resource( target, out )

    # print( pipeline.__dict__ )
    # print( os.getcwd() )
    # save_file( os.getcwd(), "sample.txt", serialise )


class Resource( object ):
    def __init__(self, metadata, data = None ):
        self._metadata = metadata
        self._data = data

    ##******************************************************************************
    ##+~ Attributes, Getter and Setter ~+##
    @property
    def data( self ):
        return self._data

    @data.setter
    def data( self, data ):
        self._data = data

    @property
    def metadata( self ):
        return self._metadata

    @metadata.setter
    def metadata( self, metadata ):
        self._metadata = metadata
    
    @property
    def name( self ):
        return self._metadata.get('name')

    @name.setter
    def name( self, name ):
        self._metadata['name'] = name
    ##******************************************************************************
    ##+~ Methods ~+##
    def convert( self, data, pipeline_config, pipeline_mode = None ):
        pipe = Pipeline( pipeline_config )
        result = pipe.run( 
            data = data, 
            mode_arguments = pipeline_mode
            )
        self.data = result
        return pipe
    
    def pipeline( self, config, **kwargs):
        self._pipeline = Pipeline( config )
        result = self._pipeline.run( 
            data = self.data, 
            mode_arguments = kwargs
            )
        return result

    def from_path( self, path ):
        data = read_file( path )
        self.data = data
    
    def to_json( self ):
        obj = { 
            "metadata"  : self.metadata,
            "data" : self.data 
        }
        return json.dumps( obj, ensure_ascii=False)

    def save( self, path ):
        name = self.name if self.name else 'untitled'
        save_file( path, name, self.data )
        return os.path.join(path, name)

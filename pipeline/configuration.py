"""
configuration.py
----------------

This module implements the Configuration class, a helper class that provides an interface to evaluate a given JSON pipeline configuration and to convert it into actual python logic.
It is used to load functions, variables, classes and modules on runtime by their names as strings.

Date    : 2019-11-08
Version : 1.1 (2020-04-24)
Author  : Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019

Modified:
    2020-04-24  : version 1.1 total refactoring of Config Class and its Subclasses.
"""

import copy, hashlib, json, os, sys, uuid
from .context import Context

## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ##
## ++       Module Functions
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ##
def base_package( package ):
    """
    Identifies the path of the main package of a sub-package
    
    Args:
        package (str|object): the name of a package or itself
        
    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2020-04-14
    """
    package_name = package if isinstance(package, str) else package.__name__
    base_package_name = package_name.split('.')[0]
    return sys.modules[ base_package_name ]

def read_manifest( manifest ):
    if isinstance( manifest, str ):
        return json.loads( manifest )
    elif isinstance( manifest, dict ):
        return manifest
    else:
        return None

def copy_dict( input_dict, ignore = [] ):
    return update_dict( input_dict, {}, ignore )

def update_dict( input_dict, update_dict, ignore = [] ):
    for key, value in input_dict.items():
        if key not in ignore:
            update_dict[ key ] = value
    return update_dict

def function_from_context( context, funct_manifest ):
    funct_name = funct_manifest.get( 'name' )
    module_name = funct_manifest.get( 'module' )
    return context.get_symbol( funct_name, module_name )


def argument_from_context( context, argument_manifest ):
    argument_name = argument_manifest.get( 'name' )
    module_name = argument_manifest.get( 'module' )
    return context.get_symbol( argument_name, module_name )


def step_config_from_manifest( context, step_manifest ):
    dispatched_function = {}
    dispatched_function['manifest'] = copy_dict( step_manifest, ignore = ["function", "function_object", "contexts"] )

    # disptach steps
    if step_manifest.get("function"):
        #print("function by name")
        dispatched_function['manifest']['function'] = step_manifest['function']
        dispatched_function['function_object'] = function_from_context( context, step_manifest['function'] )
    
    elif step_manifest.get("function_object"):
        #print("function by symbol")
        dispatched_function['function_object'] = step_manifest['function_object']
        dispatched_function['manifest']['function'] = {
            "name" : step_manifest['function_object'].__name__,
            "module" : step_manifest['function_object'].__module__
        }
    
    # disptach arguments
    arguments = step_manifest.get( 'arguments' )
    if arguments:
        dispatched_function['arguments'] = {}

        for key, value in arguments.items():
            if isinstance(value, dict) and value.get('name') and value.get('module'):
                #print('argument by name')
                dispatched_function['arguments'][ key ] = argument_from_context( context, value )
                
            else:
                #print('argument by symbol')
                dispatched_function['arguments'][ key ] = value

    #print("\n\nMANIFEST:", dispatched_function['manifest'])            
    return dispatched_function
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ##
## ++       Module Classes
## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ##

class Config(object):
    """
    Initialises a Config Instance to transform a given definition in JSON or as DICT to an actual processable pipeline configuration

    This functions is needed to convert a pipeline config into something processable. It expands function references and defines the context of functions

    Args:
        paths (*str)   : atrings representing paths to be loaded into the applications context

    Note:
        some methods of this class are somewhat quick and dirty; they need cleanup!
    
    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2020-04-21
    """
    context = Context()
    
    ##*******************************
    ##+~ Class methods 
    ##*********
    @classmethod
    def fetch_contexts( cls, manifest ):
        if manifest.get( "contexts" ):
            return manifest[ 'contexts' ]
        else:
            return []

    @classmethod
    def from_manifest( cls, manifest ):
        manifest = read_manifest( manifest )

        # fetch context paths from manifest
        context_paths = cls.fetch_contexts( manifest )

        if manifest.get( 'mode' ) or manifest.get( 'steps' ):
            return PipelineConfig( *context_paths ).from_manifest( manifest ) 
        elif manifest.get( 'function_object' ) or manifest.get( 'function' ):
            return StepConfig( *context_paths ).from_manifest( manifest ) 
        else:
            return Config( *context_paths )

    @classmethod
    def read_manifest( cls, manifest ):
        return read_manifest( manifest )
    ##*******************************
    ##+~ Instance Magic methods 
    ##*********
    def __init__( self, *paths ):
        self.add_paths( *paths )
        self._manifest = {}

    def __getitem__(self, item):
        try:
            return getattr(self, item)
        except AttributeError:
            try:
                return self._manifest[item]
            except KeyError:
                raise KeyError( self.__class__ + " object has no " + item )


    ##*******************************
    ##+~ Instance Properties 
    ##*********            
    @property
    def contexts( self ):
        return self.manifest.get( 'contexts' )
    
    @contexts.setter
    def contexts( self, contexts ):
        self.add_paths( *contexts )
        if not self.manifest.get( 'contexts' ):
            self.manifest['contexts'] = contexts

    @property
    def manifest( self ):
        return self._manifest

    @property
    def paths( self ):
        return self.context.paths
    

    ##*******************************
    ##+~ Instance Methods
    ##*********
    def add_paths( self, *paths ):
        self.context.add_paths( *paths )
    
    def to_manifest( self, config ):
        return config.to_manifest()



class PipelineConfig( Config ):
    """
    """
    ##*******************************
    ##+~ Instance Magic methods 
    ##*********
    def __init__( self, *paths ):
        super().__init__( *paths )
        self._steps = []


    ##*******************************
    ##+~ Instance Properties 
    ##*********
    @property
    def mode( self ):
        return self._manifest.get('mode')

    @mode.setter
    def mode( self, mode ):
        if mode:
            self._manifest['mode'] = mode

    @property
    def steps( self ):
        return self._steps

    @steps.setter
    def steps( self, steps ):
        if steps:
            self._steps = steps
    

    ##*******************************
    ##+~ Instance Methods
    ##*********
    def from_manifest( self, manifest ):
        # read manifest
        manifest = read_manifest( manifest )
        self._manifest = copy_dict( manifest, ignore = ['steps'] )
        
        # fetch mode
        self.mode = manifest.get('mode')
        
        # fetch context paths
        context_paths = Config.fetch_contexts( manifest )
        self.add_paths( *context_paths )
        
        # decide if pipe manifest or step manifest and dispatch it accordingly
        if manifest.get( 'steps' ):
            for step_manifest in manifest[ 'steps' ]:
                step_config = StepConfig( self.context ).from_manifest( step_manifest )
                self.steps.append( step_config )

        return self
    
    def to_manifest( self ):
        manifest = self.manifest
        manifest[ 'steps' ] = [ step.manifest for step in self.steps ]
        return manifest
        


class StepConfig( Config ):
    """
    """
    ##*******************************
    ##+~ Instance Magic methods 
    ##*********
    def __init__( self, *paths ):
        super().__init__( *paths )
        self._function_object = None
        self._arguments = {}


    ##*******************************
    ##+~ Instance Properties 
    ##*********
    @property
    def arguments( self ):
        return self._arguments if len(self._arguments) > 0 else None

    @arguments.setter
    def arguments( self, arguments ):
        if arguments:
            for key, value in arguments.items():
                if isinstance(value, dict) and value.get('name') and value.get('module'):
                    #print('argument by name')
                    
                    # fetch context paths
                    context_paths = Config.fetch_contexts( value )
                    self.add_paths( *context_paths )
                    self._arguments[ key ] = argument_from_context( self.context, value )

                else:
                    #print('argument by symbol')
                    self._arguments[ key ] = value
                
                # if not already in tht manifest put it there
                if not self.manifest.get('arguments'):
                    self.manifest['arguments'] = {}
                
                self.manifest['arguments'][ key ] = value


    @property
    def id( self ):
        if self.manifest.get('id'):
            return self.manifest['id']
        else:
            name = self.function_object.__name__ if self.function_object else ''
            return name + "_" + uuid.uuid4().hex
    
    @id.setter
    def id( self, id_nr ):
        if id:
            self._manifest[ 'id' ] = id_nr


    @property
    def function( self ):
        return self._manifest.get('function')

    @function.setter
    def function( self, function ):
        """
        SETTER: Sets the Configs's callable definition containing the manifest syntax including the callable's name and module.
        It dispatches the given manifest syntax into a callable. 
        That means that Config.function sets the _function property as well as the _function_object property within the Config.

        Args:
            function_dict   (dict)  : a dictionary containing the callable's name and module-name as strings
                
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
        Date: 2019-10-29
        """
        if function:
            self.manifest['function'] = function
            self._function_object = function_from_context( self.context, function )


    @property
    def function_object( self ):
        return self._function_object

    @function_object.setter
    def function_object( self, function_object ):
        self._function_object = function_object
        self.manifest['function'] = {
            'name' : function_object.__name__,
            'module' : function_object.__module__
        }
    

    ##*******************************
    ##+~ Instance Methods
    ##*********
    def from_manifest( self, manifest ):
        # read manifest
        manifest = read_manifest( manifest )
        self._manifest = copy_dict( manifest, ignore = ['function_object'] )

        # fetch context paths
        context_paths = Config.fetch_contexts( manifest )
        self.add_paths( *context_paths )

        # fetch function
        if manifest.get('function'):
            self.function = manifest[ 'function' ]
        elif manifest.get( 'function_object' ):
            self.function_object = manifest[ 'function_object' ]

        # fetch arguments
        self.arguments = manifest.get( 'arguments' )

        # fetch id
        #self.id = manifest.get( 'id' )
              
        return self

    def to_manifest( self ):
        return self.manifest
    
   

# class Config( object ):
#     """
#     Initialises a PipelineConfig Instance to transform a given definition in JSON or as LIST to an actual processable pipeline configuration

#     This functions is needed to convert a pipeline config into something processable. It expands function references and defines the context of functions

#     Args:
#         config (str|list)   : a config as JSON or Python list object
#         scope (dict)        : (optional) a python dictionary containing an additional symbols table like globals() or locals() to draw functions, variables etc. from. This may be used to resolve symbols by string

#     Note:
#         some methods of this class are somewhat quick and dirty; they need cleanup!
    
#     Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
#     Date: 2019-11-08
#     """
    
#     ##*******************************
#     ##+~ Magic methods 
#     ##*********
#     def __init__( self, manifest = None ):
#         if manifest:
#             self.from_manifest( manifest )
#         else:
#             self.context = None
#             self.manifest = None


#     def __repr__(self):
#         return f"<{self.__class__.__name__} manifest={self.manifest}>"    

    
#     ##*******************************
#     ##+~ Class Properties
#     ##*********
#     @property
#     def context( self ):
#         return self._context
    
#     @context.setter
#     def context( self, contexts ):
#         self._context = Context( *contexts ) if contexts else None
    

#     @property
#     def manifest( self ):
#         return self._manifest if self._manifest else {}
    
#     @manifest.setter
#     def manifest( self, manifest ):
#         self._manifest = read_manifest( manifest )

#     @property
#     def paths( self ):
#         return self.context.paths
#     ##*******************************
#     ##+~ Class Methods
#     ##*********
#     def from_manifest( self, manifest ):
#         """
#         Loads a config from a Pyton list or a JSON string

#         This functions is needed to controll the config input

#         Args:
#             config (str|list|dict)   : a config as JSON or Python list or dict object
        
#         Returns:
#             object : the dispatched config as python list or dict
        
#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
#         Date: 2019-11-08
#         """
#         self.manifest = manifest
#         self.context = self.manifest.get("contexts") 
#         return self



# ## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ##
# ## ++       Module Classes
# ## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ##
# class PipeConfig( Config ):
#     """
#     Initialises a PipelineConfig Instance to transform a given definition in JSON or as LIST to an actual processable pipeline configuration

#     This functions is needed to convert a pipeline config into something processable. It expands function references and defines the context of functions

#     Args:
#         config (str|list)   : a config as JSON or Python list object
#         scope (dict)        : (optional) a python dictionary containing an additional symbols table like globals() or locals() to draw functions, variables etc. from. This may be used to resolve symbols by string

#     Note:
#         some methods of this class are somewhat quick and dirty; they need cleanup!
    
#     Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
#     Date: 2019-11-08
#     """
    
#     ##*******************************
#     ##+~ Magic methods 
#     ##*********
#     def __init__( self, manifest = None ):
#         super().__init__( manifest )
#         if not manifest:
#             self.mode = {}


#     def __repr__(self):
#         return f"<{self.__class__.__name__} manifest={self.manifest}>"    

    
#     ##*******************************
#     ##+~ Class Properties
#     ##*********
#     @property
#     def mode( self ):
#         return self._mode
    
#     @mode.setter
#     def mode( self, mode ):
#         self._mode = mode if mode else {}  


#     @property
#     def steps( self ):
#         #return self.dispatch_steps( self.manifest.get("steps") )
#         steps = self.manifest.get( "steps" )
#         return [ StepConfig( context = self.context ).from_manifest( step_manifest ) for step_manifest in steps ] if steps else []


#     ##*******************************
#     ##+~ Class Methods
#     ##*********
#     def dispatch_step( self, funct_manifest ):
#         """
#         Resolves an individual Step configuration into an actual Step instance

#         This functions is needed to resolve a given function config to actual symbols

#         Args:
#             config_step (dict)   : a config step as Python dict with function definition and arguments
        
#         Returns:
#             dict       : a resolved step

#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
#         Date: 2019-11-08
#         """
#         funct_manifest = json.loads( funct_manifest ) if isinstance(funct_manifest, str) else funct_manifest
#         arguments = funct_manifest["arguments"] if funct_manifest.get("arguments") else {}
#         return StepDefinition( funct_manifest, arguments, self.context )


#     def dispatch_steps( self, steps = None ):
#         """
#         Resolves a list of Step configurations into an actual Step instance

#         This functions is needed to resolve a given function config to actual symbols

#         Args:
#             steps (list)   : a list containing function definitions and arguments to resolve as Steps
        
#         Returns:
#             list       : a resolved steps

#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
#         Date: 2019-11-08
#         """
#         return [ self.dispatch_step( step_manifest ) for step_manifest in steps ] if steps else []


#     def from_manifest( self, manifest ):
#         """
#         Loads a config from a Pyton list or a JSON string

#         This functions is needed to controll the config input

#         Args:
#             config (str|list|dict)   : a config as JSON or Python list or dict object
        
#         Returns:
#             object : the dispatched config as python list or dict
        
#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
#         Date: 2019-11-08
#         """
#         self.manifest = manifest
#         self.mode = self.manifest.get("mode")
#         self.context = self.manifest.get("contexts") 



# class StepDefinition( object ):
#     """
#     A Definition Class for Steps providing a structured and guided configuration

#     Args:
#         manifest (str|object)   : a config as JSON string or Python dict object
#         arguments (dict)        : (optional) a python dictionary containing arguments to the defined function
#         context                 : (optional) the context object to dispatch function names to objects

    
#     Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
#     Date: 2020-04-09
#     """

#     ##*******************************
#     ##+~ Magic methods 
#     ##*********
#     def __init__( self, manifest, arguments = {}, context = None ):
#         self.manifest = copy.deepcopy(manifest)
#         self._callable = self.dispatch_callable( manifest, context )
#         self._arguments = self.dispatch_arguments( arguments, context )

#     def __repr__(self):
#         return f"<{self.__class__.__name__} id={self.id} callable={self.callable} arguments={self.arguments}, manifest>"


#     ##*******************************
#     ##+~ Class Properties
#     ##*********
#     @property
#     def id( self ):
#         """
#         GETTER: the id of a step provided in the manifest
        
#         Returns:
#             str       : the id 

#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
#         Date: 2020-04-12
#         """
#         if self.manifest.get('id'):
#             return self.manifest['id']
#         else:
#             callable_name = self._callable.__name__
#             return callable_name + "_" + uuid.uuid4().hex

#     @property
#     def callable( self ):
#         """
#         GETTER: the function as python callable dispatched from the manifests information
        
#         Returns:
#             callable       : the function pointer 

#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
#         Date: 2020-04-12
#         """
#         return self._callable

#     @property
#     def arguments( self ):
#         """
#         GETTER: the function arguments dispatched from the manifests information
        
#         Returns:
#             dict       : the arguments as a python dict 

#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
#         Date: 2020-04-12
#         """
#         return self._arguments


#     ##*******************************
#     ##+~ Class Methods
#     ##*********
#     def dispatch_callable( self, manifest, context ):
#         """
#         Resolves an individual Step configuration into an actual Step instance

#         This functions is needed to resolve a given function config to actual symbols

#         Args:
#             manifest (dict)   : a configuration of a step as Python dict with function definition and arguments
#             context           : the context object to dispatch function names to objects

#         Returns:
#             callable       : a dispatched callable object

#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
#         Date: 2020-04-10
#         """
#         if manifest.get("function"):
#             func_def = manifest["function"]

#             return context.get_symbol( 
#                 func_def["name"], 
#                 func_def.get("module") 
#                 ) 

#         elif manifest.get("function_object"):
#             return manifest["function_object"] 
        
#         else:
#             return None

#     def dispatch_arguments( self, arguments, context ):
#         """
#         Resolves an arguments dict from the single step configuration into a symbolf if needed

#         This functions is needed to resolve a given symbol

#         Args:
#             arguments (dict)  : a config arguments object as Python dict with argument names and values
#             context           : the context object to dispatch function names to objects
        
#         Returns:
#             dict       : a dispatched arguments dictionary
        
#         Note:
#             this method is somewhat quick and dirty and needs cleanup
        
#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
#         Date: 2020-04-10
#         """
#         arguments_dict = {}
#         for key, value in arguments.items():
#             if isinstance(value, dict) and value.get("module"):
#                 # print("isreference")
#                 try:
#                     arguments_dict[key] = context.get_symbol( 
#                         value.get("name"), 
#                         value.get("module") 
#                         )
#                 except AttributeError as error:
#                     print("Config.resolve_step::Arguments::AttributeError: " + error.args[0])
#                     arguments_dict[key] = value
                
#             else:
#                 # print("isvalue")
#                 arguments_dict[key] = value
#         return arguments_dict
     


# class StepConfig( Config ):
#     """
#     A Definition Class for Steps providing a structured and guided configuration

#     Args:
#         manifest (str|object)   : a config as JSON string or Python dict object
#         arguments (dict)        : (optional) a python dictionary containing arguments to the defined function
#         context                 : (optional) the context object to dispatch function names to objects

    
#     Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
#     Date: 2020-04-09
#     """

#     ##*******************************
#     ##+~ Magic methods 
#     ##*********
#     def __init__( self, context, manifest = None, arguments = {} ):
#         super().__init__( manifest )
#         #self._callable = self.dispatch_callable( manifest, context )
#         #self._arguments = self.dispatch_arguments( arguments, context )
#         self._callable = ""
#         self._arguments = ""


#     def __repr__(self):
#         return f"<{self.__class__.__name__} id={self.id} callable={self.callable} arguments={self.arguments}, manifest>"


#     ##*******************************
#     ##+~ Class Properties
#     ##*********
#     @property
#     def id( self ):
#         """
#         GETTER: the id of a step provided in the manifest
        
#         Returns:
#             str       : the id 

#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
#         Date: 2020-04-12
#         """
#         if self.manifest.get('id'):
#             return self.manifest['id']
#         else:
#             callable_name = self._callable.__name__
#             return callable_name + "_" + uuid.uuid4().hex

#     @property
#     def callable( self ):
#         """
#         GETTER: the function as python callable dispatched from the manifests information
        
#         Returns:
#             callable       : the function pointer 

#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
#         Date: 2020-04-12
#         """
#         return self._callable

#     @property
#     def arguments( self ):
#         """
#         GETTER: the function arguments dispatched from the manifests information
        
#         Returns:
#             dict       : the arguments as a python dict 

#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
#         Date: 2020-04-12
#         """
#         return self._arguments


#     ##*******************************
#     ##+~ Class Methods
#     ##*********
#     def dispatch_callable( self, manifest, context ):
#         """
#         Resolves an individual Step configuration into an actual Step instance

#         This functions is needed to resolve a given function config to actual symbols

#         Args:
#             manifest (dict)   : a configuration of a step as Python dict with function definition and arguments
#             context           : the context object to dispatch function names to objects

#         Returns:
#             callable       : a dispatched callable object

#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
#         Date: 2020-04-10
#         """
#         if manifest.get("function"):
#             func_def = manifest["function"]

#             return context.get_symbol( 
#                 func_def["name"], 
#                 func_def.get("module") 
#                 ) 

#         elif manifest.get("function_object"):
#             return manifest["function_object"] 
        
#         else:
#             return None

#     def dispatch_arguments( self, arguments, context ):
#         """
#         Resolves an arguments dict from the single step configuration into a symbolf if needed

#         This functions is needed to resolve a given symbol

#         Args:
#             arguments (dict)  : a config arguments object as Python dict with argument names and values
#             context           : the context object to dispatch function names to objects
        
#         Returns:
#             dict       : a dispatched arguments dictionary
        
#         Note:
#             this method is somewhat quick and dirty and needs cleanup
        
#         Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
#         Date: 2020-04-10
#         """
#         arguments_dict = {}
#         for key, value in arguments.items():
#             if isinstance(value, dict) and value.get("module"):
#                 # print("isreference")
#                 try:
#                     arguments_dict[key] = context.get_symbol( 
#                         value.get("name"), 
#                         value.get("module") 
#                         )
#                 except AttributeError as error:
#                     print("Config.resolve_step::Arguments::AttributeError: " + error.args[0])
#                     arguments_dict[key] = value
                
#             else:
#                 # print("isvalue")
#                 arguments_dict[key] = value
#         return arguments_dict
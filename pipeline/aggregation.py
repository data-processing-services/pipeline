#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Uwe Sikora <sikora@sub.uni-goettingen.de>
# Licensed under the ...

"""
aggregation.py
-------

This module implements the main classes of aggrePY 

Date    : 2020-03-28
Version : 1.0
Author  : Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
"""

import glob, json, os, pathlib, pprint, requests, shutil, sys
from collections import UserDict
from urllib.parse import urlparse
from smart_open import open  # for transparently opening remote files
from .pipeline import Pipeline
from .interfaces import save_file


class Resource( object ):
    """
    A Ressource contains represents a data object
    
    Args:
        name (str)   : (optional) a python dictionary containing Step configurations to initialise the Pipeline with

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2020-03-28
    """
    ##*******************************
    ##+~ Magic Instance methods 
    ##*********
    def __init__( self, path = None, name = None ):
        self.metadata = {}
        self.name = name
        self.path = path
        self.type = self.__class__.__name__
    
    def __repr__( self ):
        return f"<{self.__class__.__name__} { self.json() }>"


    ##*******************************    
    ##+~  Static Methods
    ##*********
    @staticmethod
    def from_path( path, name = None ):
        """constructor method from path which determins the type of a path from its path as long as the resource exists"""
        if Resource.isdir( path ):
            res = Aggregation.from_dir( path, name )
        elif Resource.isurl( path ):
            res = WebResource.from_url( path, name )
        elif Resource.isfile( path ):
            res = FileResource.from_path( path, name )
        else:
            res = Resource( path, name )
            
        return res
    
    @staticmethod
    def from_manifest( manifest ):
        """creates a Resource from a manifest"""
        res = Resource.init( manifest["type"], manifest["path"], manifest.get("name") ) 
        return res
    
    @staticmethod
    def init( class_name, path = None, name = None ):
        """Resource classes constructor"""
        if class_name == 'Aggregation':
            res = Aggregation( path, name )
        elif class_name == 'WebResource':
            res = WebResource( path, name ) 
        else:
            res = FileResource( path, name )
        return res
    
    @staticmethod
    def isurl( path ):
        """checks if a given path is a URL"""
        return urlparse( path ).scheme != ""
    
    @staticmethod
    def isfile( path ):
        """checks if a given path exists and is a file"""
        return os.path.isfile( path )
    
    @staticmethod
    def isdir( path ):
        """checks if a given path exists and is a dir"""
        return os.path.isdir( path )

    
    ##*******************************
    ##+~ Instance Properties
    ##*********
    @property
    def dict( self ):
        return {
            "name" : self.name,
            "type" : self.type,
            "path" : self.path
        }
    
    @property
    def exists( self ):
        """checks if a Resource exists or is accessible"""
        check = False
        # Determin if resource have already a path asigned and test
        if self.path:
            
            # if the resource is a webresource determin if it is accesable, else return False
            if self.isurl( self.path ):
                req = requests.get( self.path )
                status_code = req.status_code
                check = True if status_code == 200 else False
                
            # check if the resource - wether it is a file or a dir - exists
            elif self.isfile( self.path ) or self.isdir( self.path ):
                check = True
        else:
            check = False
        return check
    
    @property
    def manifest( self ):
        """creates a manifest for the resource"""
        return self.json() 
    
    @property
    def name( self ):
        """
        The name of the resource

        Returns:
            boolean : status of the pipeline instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-03-28
        """
        if self._name in ['untitled', None] and self.resourcename not in ['']:
            self._name = self.resourcename
        return self._name
    
    @name.setter
    def name( self, name ):
        """
        Sets the name attribute in the resources metadata

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-03-28
        """
        self._name = name if name else 'untitled'

    @property
    def metadata( self ):
        """
        The metadata of the resource

        Returns:
            boolean : status of the pipeline instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-03-28
        """
        return self._metadata

    @metadata.setter
    def metadata( self, metadata = {} ):
        """
        Sets the metadata attribute of the resource

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-03-28
        """
        self._metadata = metadata
        
    @property
    def resourcename( self ):
        """the name of the resource"""
        if self.path:
            if self.isurl( self.path ):
                parsed = urlparse( self.path )
                return parsed.netloc + '_' + "_".join( filter(None, parsed.path.split("/")) )
            else:
                base = os.path.basename( self.path )
                return os.path.splitext( base )[0]
        else:
            return ''
    
    @property
    def path( self ):
        """
        The path of the resource

        Returns:
            boolean : status of the pipeline instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-03-28
        """
        return self._path

    @path.setter
    def path( self, path ):
        """
        Sets the path attribute

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-03-28
        """
        if not path:
            self._path = None
        elif self.isurl( path ):
            self._path = path
        else:
            self._path = os.path.abspath( path )
    
    
    ##*******************************
    ##+~ Instance Methods
    ##*********
    def create( self, data = None ):
        print("create")
    
    
    def json( self ):
        return json.dumps( self.dict )
        
    def open( self ):
        try:
            return open( self.path )
        except IsADirectoryError:
            pass
        
    def rm( self ):
        """removes a resource from the filesystem."""
        os.remove( self.path )



class Aggregation( Resource ):
    """
    An Aggregation contains references to multiple resources and provides methods to access them.
    
    Args:
        path (str)   : (optional) a path to the aggregation on the filesystem
        name (str)   : (optional) a custom name for the aggregation

    Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2019
    Date: 2020-02-28
    """
    ##*******************************
    ##+~ Magic Instance Methods
    ##*********
    def __init__( self, path = None, name = None ):
        "class constructor"
        super().__init__( path, name )
        self.resources = []
    
    def __iter__( self ):
        """iterator that holds all documents of the aggregation memory efficiantly."""
        for document in self.resources:
            yield document
    
    def __repr__( self ):
        return f"<{self.__class__.__name__} (type={self.type}, name={self.name}, path={self.path}, resources={self.resources})>"
    

    ##*******************************
    ##+~ Static Methods
    ##*********
    @staticmethod
    def from_manifest( manifest ):
        pass
    
    @staticmethod
    def from_dir( path, name = None ):
        """creates an aggregation from a given path and the files and directories in it."""
        aggregation = Aggregation( path, name )
        aggregation.readdir()
        return aggregation
    
    
    ##*******************************
    ##+~ Instance Properties
    ##*********  
    @property
    def dict( self ):
        """the aggregation object as a simple python dictionary."""
        super_dict = super().dict
        super_dict["resources"] = [resource.dict for resource in self.resources]
        return super_dict
    
    @property
    def length( self ):
        """the length of the aggregation aka. the number of aggregated resources."""
        return len( self._resources )
    
    @property
    def resources( self ):
        return self._resources
    
    @resources.setter
    def resources( self, lst ):
        """setter function to populate the resources property"""
        # check if _resources already exists
        if not hasattr( self, "_resources") or not self._resources:
            self._resources = []
            
        for item in lst:
            self.add_resource( item )
    

    ##*******************************
    ##+~ Instance Methods
    ##*********
    def all_resources( self ):
        for res in self.resources: 
            if res.type == "Aggregation":
                yield res
                for rec_res in res.all_resources():
                    yield rec_res
            else:
                yield res
        
        
    def aggregations( self, path = None, name = None, recursive = False  ):                
        resources = self.all_resources() if recursive else self.resources
        try:
            if name and path:
                return [ agg for agg in resources if agg.type == "Aggregation" and agg.name == name and agg.path == path][0]
            elif name:
                return [ agg for agg in resources if agg.type == "Aggregation" and agg.name == name ][0]
            elif path:
                return [ agg for agg in resources if agg.type == "Aggregation" and agg.path == path][0]
            else:
                new_list = sorted([ agg for agg in resources if isinstance(agg, Aggregation) ], key=lambda x: x.path, reverse=True)
                return new_list 
        except IndexError:
            return []
    
    def files( self, path = None, name = None, recursive = False ):
        resources = self.all_resources() if recursive else self.resources
        try:
            if name and path:
                return [ res for res in resources if isinstance(res, FileResource) and res.name == name and res.path == path][0]
            elif name:
                return [ res for res in resources if isinstance(res, FileResource) and res.name == name ][0]
            elif path:
                return [ res for res in resources if isinstance(res, FileResource) and res.path == path][0]
            else:
                new_list = sorted([ res for res in resources if isinstance(res, FileResource) ], key=lambda x: x.path, reverse=True)
                return new_list 
        except IndexError:
            return []
    
    # ! WARNING: Needs testing
    def add_resource( self, resource ):
        """
        Adds a Resource to the Aggregation

        Args:
            object (dict)   : a Step Definition Dict containing "name" (string), "function_object" (function object), "arguments" ( a list containing key:value pairs ).
        
        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-03-28
        """
        if not isinstance( resource, Resource ):
            resource = Resource.init( 
                    resource["type"], 
                    resource["path"], 
                    resource.get("name")
            )
        
        if not any(res.path == resource.path for res in self._resources):
            self._resources.append( resource )
        
        return self.resources
        
    def clear( self ):
        """clears the aggregations from all aggregated resources."""
        self.resources = []
        return self.resources
    
    def listdir( self, flag = "*"):
        """lists all files and directories in the path."""
        return glob.glob( os.path.join( self.path, flag ) ) if self.exists else []
       
    # ! WARNING: Needs testing
    def mkdir( self ):
        """creates a path tree from self.path on the filesystem if it not already exists."""
        if self.path:
            pathlib.Path( self.path ).mkdir( parents=True, exist_ok=True )
        else:
            print("Error: path is not set!")
        
    def open( self ):
        """opens the aggregation as multiple document objects, line by line per document."""
        #for document in self.__iter__():
        #    yield document
        for document in self.resources:
            if not document.type == self.__class__.__name__: 
                yield document.open()
            else:
                for doc in document.open():
                    yield doc
    
    def readdir( self ):
        """reads in all files and directories from self.path on the filesystem and creates objects"""
        for resource in self.listdir():
            self.add_resource( Resource.from_path(resource ) )
        return self.resources
    
    # ! WARNING: Needs testing
    def rm( self ):
        """removes an aggregation and its path from the filesystem."""
        if self.path:
            shutil.rmtree( self.path )
        else:
            print("Error: path is not set!")
    
    # ! WARNING: Needs testing
    def to_disk( self ):
        """save aggregation to self.path on the filesystem."""
        if self.path:
            pass
        else:
            print("Error: path is not set!")
    
    def content( self ):
        """represents all documents of the aggregation as joined lines memory efficiantly."""
        for resource in self.resources:
            if not resource.type == self.__class__.__name__:
                yield resource.content()
            else:
                for document in resource.content():
                    yield document
            
    def lines( self ):
        """merges all resources documents together and represents all lines of all documents line by line"""
        for document in self.open():
            for line in document:
                yield line



class FileResource( Resource ):
    """"""
    ##*******************************
    ##+~ Magic Instance Methods
    ##*********
    def __init__( self, path = None, name = None ):
        super().__init__( path, name )
        self.pipeline = Pipeline()
    
    def __repr__( self ):
        return f"<{self.__class__.__name__} (type={self.type}, name={self.name}, path={self.path})>"
    
    
    ##*******************************
    ##+~ Static Methods
    ##*********
    @staticmethod
    def from_path( path, name = None ):
        file_resource = FileResource( path, name )
        return file_resource
    
    
    ##*******************************
    ##+~ Instance Properties
    ##*********
    @property
    def dict( self ):
        return {
            "name" : self.name,
            "type" : self.type,
            "path" : self.path
        }

    @property
    def dir( self ):
        return os.path.dirname( self.path )

    @property
    def file_extension( self ):
        elements = os.path.splitext( self.path )
        return [e for e in elements[1:]] if len(elements) > 1 else None

    
    ##*******************************
    ##+~ Instance Methods
    ##*********            
    def lines( self ):
        for line in self.open():
            yield line
        
    def content( self ):
        return "".join( self.lines() )
    
    def create_derivate( self, data, path = None, name = None ):
        #print(os.path.abspath(path))
        #print(self.dir)
        #print(os.path.abspath(path) != self.dir)
        path = os.path.abspath(path) if path and os.path.abspath(path) != self.dir else os.path.join(self.dir, "derivates")
        #print(path)
        name = name if name else self.name
        save_file(
            path = path,
            name = name,
            data = data
        )
        return FileResource( path = path, name = name )
    

class WebResource( FileResource ):
    """"""
    ##*******************************
    ##+~ MagicInstance Methods
    ##*********
    def __init__( self, path = None, name = None ):
        super().__init__( path, name )
    

    ##*******************************
    ##+~ Static Methods
    ##*********
    @staticmethod
    def from_url( url, name = None ):
        web_resource = WebResource( url, name )
        return web_resource
    
    
    ##*******************************
    ##+~ Instance Properties
    ##*********
    
    
    ##*******************************
    ##+~ Instance Methods
    ##*********
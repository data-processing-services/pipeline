# pipeline/__init__.py
from .pipeline import Pipeline, Step
from .context import Context, main_script, get_module
from .configuration import Config, PipelineConfig, StepConfig, base_package, read_manifest
from .errors import Error, ProcessingError, PipelineError
from .interfaces import process_resource, process_collection, process_from_config, read_dir, read_file, from_dir, pickle_load, pickle_save, Resource
from .corpus import Corpus
from .aggregation import Resource, Aggregation, FileResource, WebResource
from .serialiser import Serialiser
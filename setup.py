from setuptools import setup

setup(name='pipeline',
      version='1.0.4',
      description='defining processing pipelines within python scripts',
      url='https://gitlab.gwdg.de/data-processing-services/pipeline',
      author='Uwe Sikora',
      author_email='sikora@sub.uni-goettingen.de',
      license='MIT',
      packages=['pipeline'],
      install_requires=['Click', 'nltk', 'requests', 'smart-open', 'jupyter'], #external packages as dependencies
      zip_safe=False)
